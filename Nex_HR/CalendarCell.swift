//
//  CalendarCell.swift
//  Nex_HR
//
//  Created by Daud on 10/26/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class CalendarCell: UICollectionViewCell {
    
    let lbl = UILabel()
    let leaveMark = UIView()
    
    func initialize(vc : ViewController, lblStr : String, today : Bool, leaveColor : UIColor)
    {
        for ss in self.subviews
        {
            ss.removeFromSuperview()
        }
        
        self.backgroundColor = UIColor.clear
        lbl.text = lblStr
        lbl.textColor = UIColor.black
        lbl.font = UIFont(name: "SFUIText-Medium", size: 16)
        lbl.textAlignment = .center
        lbl.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        lbl.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        lbl.layer.cornerRadius = lbl.frame.height / 2
        lbl.layer.borderColor = UIColor.black.cgColor
        if (today)
        {
            lbl.layer.borderWidth = 1.5
        }
        else
        {
            lbl.layer.borderWidth = 0
        }
        self.addSubview(lbl)
        
        leaveMark.frame = CGRect(x: self.frame.width - 12, y: 6, width: 6, height: 6)
        leaveMark.layer.cornerRadius = leaveMark.frame.width / 2
        leaveMark.alpha = 0
        self.addSubview(leaveMark)
        
        if (leaveColor != UIColor.blue)
        {
            leaveMark.alpha = 1
            leaveMark.backgroundColor = leaveColor
        }
        
//        if (leave == "casual")
//        {
//            leaveMark.alpha = 1
//            leaveMark.backgroundColor = UIColor(red: 0, green: 204/255, blue: 126/255, alpha: 1)
//        }
//        else if (leave == "medicine")
//        {
//            leaveMark.alpha = 1
//            leaveMark.backgroundColor = UIColor(red: 0/255, green: 180/255, blue: 218/255, alpha: 1)
//        }
//        else if (leave == "annual")
//        {
//            leaveMark.alpha = 1
//            leaveMark.backgroundColor = UIColor(red: 255/255, green: 175/255, blue: 0/255, alpha: 1)
//        }
//        else if (leave == "other")
//        {
//            leaveMark.alpha = 1
//            leaveMark.backgroundColor = UIColor(red: 255/255, green: 109/255, blue: 109/255, alpha: 1)
//        }
//        else if (leave == "pending")
//        {
//            leaveMark.alpha = 1
//            leaveMark.backgroundColor = UIColor(red: 187/255, green: 187/255, blue: 187/255, alpha: 1)
//        }
//        else if (leave == "none")
//        {
//            leaveMark.alpha = 0
//        }
    }
    
}
