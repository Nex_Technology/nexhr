//
//  ManualCheckIn.swift
//  Nex_HR
//
//  Created by Daud on 4/4/17.
//  Copyright © 2017 Daud. All rights reserved.
//

import Foundation
import Alamofire
import UIKit

class ManualView : UIView, UITableViewDelegate, UITableViewDataSource
{
    var vc : ViewController!
    
    let tabView = UITableView()
    let headerCover = UIView()
    
    let sideAlignment : CGFloat = 20
    
    var reasonInit = "Reason"
    var curApprovedBy = "SomeOne"
    var curApprovedById = ""
    var curReason = ""
    var approvedBys = [String]()
    var approvedByIDs = [String]()
    
    var txtColor = UIColor.gray
    var curReasonColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
    
    func initialize(vc : ViewController)
    {
        self.vc = vc
        
        for var i in (0 ..< vc.userInform.ApprovedByNames.count)
        {
            approvedBys.append("\(vc.userInform.ApprovedByNames[i]) (\(vc.userInform.ApprovedByJobs[i]))")
        }
        approvedByIDs = vc.userInform.ApprovedByIDs
        curApprovedBy = approvedBys[0]
        curApprovedById = approvedByIDs[0]
        
        tabView.dataSource = self
        tabView.delegate = self
        tabView.register(SubmitCell.self, forCellReuseIdentifier: "submitCell")
        tabView.register(TextViewCell.self, forCellReuseIdentifier: "textViewCell")
        tabView.register(SelecterCell.self, forCellReuseIdentifier: "selecterCell")
        tabView.frame = CGRect(x: sideAlignment, y: 120, width: self.frame.width - sideAlignment * 2, height: self.frame.height - 120)
        tabView.separatorColor = UIColor.clear
        tabView.showsVerticalScrollIndicator = false
        tabView.delaysContentTouches = false
        self.addSubview(tabView)
        
        headerCover.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 1
        self.addSubview(headerCover)
        
        let lblForm = UILabel()
        lblForm.text = "Manual Form"
        lblForm.textColor = UIColor.black
        lblForm.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblForm.sizeToFit()
        lblForm.frame.origin.x = sideAlignment
        lblForm.center.y = 50
        headerCover.addSubview(lblForm)
        
        let btnCancle = UIButton(frame: CGRect(x: sideAlignment, y: 0, width: 100, height: 100))
        btnCancle.setTitle("Cancel", for: .normal)
        btnCancle.setTitleColor(UIColor(red : 0/255, green : 175/255, blue : 255/255, alpha : 1), for: .normal)
        btnCancle.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 18)
        btnCancle.sizeToFit()
        btnCancle.frame.origin.x = self.frame.width - btnCancle.frame.width - sideAlignment
        btnCancle.center.y = 50
        headerCover.addSubview(btnCancle)
        btnCancle.addTarget(self, action: #selector(self.goBack(_:)), for: .touchUpInside)
        
    }
    
    func submitForm()
    {
        vc.loadingLabelUp(label: "Sending..")
        self.endEditing(true)
        print("submitting")
        
        let parameters = [
            "employee_id" : vc.userInform.id,
            "reason" : curReason,
            "approved_by_id" : curApprovedById,
            "check_type" : "\(UserDefaults.standard.integer(forKey: "check_type"))"
        ]
        
        print(parameters)
        
        Alamofire.request("\(BASE_URL)check_in_out_forms?auth_token=\(vc.userInform.auth_token)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.vc.loadingView.succeeded()
                    self.goBack(UIButton())
                    
                case .failure(let error) :
                    self.vc.loadingView.failed()
                    print(" ERROR IS \(error)")
                }
        }
    }
    
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 3
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0) //selectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : "Approved By", btn2 : curApprovedBy, vc : self.vc, parentView : "manualView", cellNum : indexPath.row, leaveList : approvedBys, label : "Choose Leave Type?", idList : approvedByIDs)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 1) //TextViewCell-------
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath as IndexPath) as! TextViewCell
            cell.initialize(vc : self.vc, lbl2 : "Reason", parentClass : "manualView", cellNum : indexPath.row, txtColor : curReasonColor)
            cell.selectionStyle = .none
            
            return cell
        }
        else //Submit Cell---------
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell", for: indexPath as IndexPath) as! SubmitCell
            cell.initialize(vc : self.vc, parentClass : "manualView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
    
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 1)
        {
            return 100
        }
        else
        {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.endEditing(true)
    }
    
    //Go back-----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func goBack(_ btn : UIButton!)
    {
        self.endEditing(true)
        vc.checkIn.comeBack()
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.frame.height
            
        }, completion: {finish in
            
            self.removeFromSuperview()
            
        })
        
    }
}
