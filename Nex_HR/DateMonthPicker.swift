//
//  DateMonthPicker.swift
//  Nex_HR
//
//  Created by Daud on 10/5/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class MonthYearPicker: UIView, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    var sideAlignment : CGFloat = 20
    
    var vc : ViewController!
    var monthCells = [PickerCell]()
    var yearCells = [PickerCell]()
    
    var pickerView = UIView()
    var monthTabView = UITableView()
    var yearTabView = UITableView()
    let btnChoose = UIButton(type : .system)
    
    var months = [String]()
    var years = [String]()
    var curMonthIndex = 0
    var curYearIndex = 0
    var parentView = ""
    
    func initialize(_ vc : ViewController, parentView : String)
    {
        self.vc = vc
        self.parentView = parentView
        self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTapped(_:)))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
        months = vc.userInform.months
        years = vc.userInform.years
        
        if(parentView == "PayRollView")
        {
            curMonthIndex = vc.payRollView.curMonthIndex
            curYearIndex = vc.payRollView.curYearIndex
        }
        else if(parentView == "LeaveView")
        {
            curMonthIndex = vc.leaveView.curMonthIndex
            curYearIndex = vc.leaveView.curYearIndex
        }
        else if(parentView == "AttendanceView")
        {
            curMonthIndex = vc.attendenceView.curMonthIndex
            curYearIndex = vc.attendenceView.curYearIndex
        }
        else if(parentView == "OvertimeView")
        {
            curMonthIndex = vc.overtimeView.curMonthIndex
            curYearIndex = vc.overtimeView.curYearIndex
        }
        
        pickerView.frame = CGRect(x: self.frame.width / 2, y: self.frame.height, width: self.frame.width - (sideAlignment * 2), height: 450)
        pickerView.backgroundColor = UIColor.clear
        self.addSubview(pickerView)
        
        let lblChoose = UILabel(frame : CGRect(x: 0, y: 20, width: 0, height: 0))
        lblChoose.text = "Choose month and year"
        lblChoose.textAlignment = .center
        lblChoose.textColor = UIColor.black
        lblChoose.alpha = 0.4
        lblChoose.font = UIFont(name: "SFUIText-Regular", size: 18)
        lblChoose.sizeToFit()
        pickerView.addSubview(lblChoose)
        
        btnChoose.setTitle("Choose", for: .normal)
        btnChoose.setTitleColor(UIColor.black, for: .normal)
        btnChoose.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnChoose.sizeToFit()
        btnChoose.frame.origin.y = 350
        btnChoose.frame.origin.x = 0
        btnChoose.addTarget(self, action: #selector(self.confirmDate(_:)), for: .touchUpInside)
        btnChoose.tintColor = UIColor.white
        pickerView.addSubview(btnChoose)
        
        monthTabView.frame = CGRect(x: 0, y: 70, width: pickerView.frame.width / 2, height: 250)
        monthTabView.register(PickerCell.self, forCellReuseIdentifier: "monthCell")
        monthTabView.delegate = self
        monthTabView.dataSource = self
        monthTabView.separatorColor = UIColor.clear
        monthTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(monthTabView)
        
        yearTabView.frame = CGRect(x: pickerView.frame.width / 2, y: 70, width: pickerView.frame.width / 2, height: 250)
        yearTabView.register(PickerCell.self, forCellReuseIdentifier: "yearCell")
        yearTabView.delegate = self
        yearTabView.dataSource = self
        yearTabView.separatorColor = UIColor.clear
        yearTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(yearTabView)
        
        pickerView.transform = CGAffineTransform(scaleX: 0, y: 0)
        pickerView.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            
            self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            self.pickerView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.pickerView.center = CGPoint(x : self.frame.width / 2 , y : self.frame.height / 2)
            self.pickerView.alpha = 1
            
            if(parentView == "PayRollView")
            {
                self.vc.payRollView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            }
            else if(parentView == "LeaveView")
            {
                self.vc.leaveView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            }
            else if(parentView == "AttendanceView")
            {
                self.vc.attendenceView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            }
            else if(parentView == "OvertimeView")
            {
                self.vc.overtimeView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            }
            self.pickerView.center.y = self.vc.view.center.y
            
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: {
            
        }, completion: {finish in
            
            self.monthTabView.scrollToRow(at: NSIndexPath(row: self.curMonthIndex, section: 0) as IndexPath, at: .middle, animated: true)
            self.yearTabView.scrollToRow(at: NSIndexPath(row: self.curYearIndex, section: 0) as IndexPath, at: .middle, animated: true)
            
        })
        
    }
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == monthTabView)
        {
            return 12
        }
        else
        {
            return years.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView == monthTabView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "monthCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize(months[indexPath.row],tag : indexPath.row, color : curMonthIndex == indexPath.row)
            monthCells.append(cell)
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "yearCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize("\(years[indexPath.row])",tag : indexPath.row, color : curYearIndex == indexPath.row)
            yearCells.append(cell)
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // set current month and year when the table cells are tappped------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        if(tableView == monthTabView)
        {
            for cc in monthCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curMonthIndex = indexPath.row
            print(curMonthIndex)
        }
        else
        {
            for cc in yearCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curYearIndex = indexPath.row
            print(curYearIndex)
        }
    }
    
    //Choose Date-------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func confirmDate(_ btn : UIButton)
    {
        if (parentView == "PayRollView")
        {
            vc.payRollView.curMonthIndex = curMonthIndex
            vc.payRollView.curYearIndex = curYearIndex
            vc.payRollView.curMonth = vc.userInform.months[curMonthIndex]
            vc.payRollView.curYear = vc.userInform.years[curYearIndex]
            vc.payRollView.curMonthNum = vc.userInform.monthsNum[curMonthIndex]
            vc.payRollView.curYearNum = vc.userInform.yearsNum[curYearIndex]
            vc.payRollView.changeButtonDate()
        }
        else if (parentView == "LeaveView")
        {
            vc.leaveView.curMonthIndex = curMonthIndex
            vc.leaveView.curYearIndex = curYearIndex
            vc.leaveView.curMonth = vc.userInform.months[curMonthIndex]
            vc.leaveView.curYear = vc.userInform.years[curYearIndex]
            vc.leaveView.curMonthNum = vc.userInform.monthsNum[curMonthIndex]
            vc.leaveView.curYearNum = vc.userInform.yearsNum[curYearIndex]
            vc.leaveView.changeButtonDate()
        }
        else if (parentView == "AttendanceView")
        {
            vc.attendenceView.curMonthIndex = curMonthIndex
            vc.attendenceView.curYearIndex = curYearIndex
            vc.attendenceView.curMonth = vc.userInform.months[curMonthIndex]
            vc.attendenceView.curYear = vc.userInform.years[curYearIndex]
            vc.attendenceView.curMonthNum = vc.userInform.monthsNum[curMonthIndex]
            vc.attendenceView.curYearNum = vc.userInform.yearsNum[curYearIndex]
            vc.attendenceView.changeButtonDate()
        }
        else if (parentView == "OvertimeView")
        {
            vc.overtimeView.curMonthIndex = curMonthIndex
            vc.overtimeView.curYearIndex = curYearIndex
            vc.overtimeView.curMonth = vc.userInform.months[curMonthIndex]
            vc.overtimeView.curYear = vc.userInform.years[curYearIndex]
            vc.overtimeView.curMonthNum = vc.userInform.monthsNum[curMonthIndex]
            vc.overtimeView.curYearNum = vc.userInform.yearsNum[curYearIndex]
            vc.overtimeView.changeButtonDate()
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
            if(self.parentView == "PayRollView")
            {
                self.vc.payRollView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "LeaveView")
            {
                self.vc.leaveView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "AttendanceView")
            {
                self.vc.attendenceView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "OvertimeView")
            {
                self.vc.overtimeView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
        
    }
    
    //Cancle Picking----------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func backgroundTapped(_ gesture : UITapGestureRecognizer)
    {
//        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
//            
//            self.pickerView.center.y = self.vc.view.frame.height + self.pickerView.frame.height / 2
//            self.blurView.effect = nil
//            
//            }, completion: {finish in
//                
//                UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
//                    
//                    //self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0)
//                    
//                    }, completion: {finish in
//                        
//                        self.removeFromSuperview()
//                        
//                })
//                
//        })
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
            if(self.parentView == "PayRollView")
            {
                self.vc.payRollView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "LeaveView")
            {
                self.vc.leaveView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "AttendanceView")
            {
                self.vc.attendenceView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "OvertimeView")
            {
                self.vc.overtimeView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
        
    }
    
    //Gesture View Delegate Methods-------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view != self && touch.view != pickerView && touch.view != monthTabView && touch.view != yearTabView) {
            return false
        }
        else
        {
            return true
        }
    }

}
