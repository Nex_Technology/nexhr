//
//  Calendar viewCollectionViewController.swift
//  Nex_HR
//
//  Created by Daud on 10/26/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit


class CalendarView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    var vc : ViewController!
    let days : [String] = ["S","M","T","W","T","F","S"]
    
    var monthDays : [Int] = [31,28,31,30,31,30,31,31,30,31,30,31]
    var numOfDays = 0
    var startDay = 0
    
    var leaveColors = [UIColor]()
    var leaveDays = [String]()
    var leaveTags = [Int]()

    var colView : UICollectionView!
    
    func initialize(vc : ViewController, leaveColors : [UIColor], leaveTags : [Int], leaveDays : [String])
    {
        self.vc = vc
        self.leaveColors = leaveColors
        self.leaveTags = leaveTags
        self.leaveDays = leaveDays
        self.backgroundColor = UIColor.white
        
        calculateDays()
        
        let collectionCellWidth = self.frame.width / 7
        let collectionCellHeight = self.frame.width / 7
        
        let layout = UICollectionViewFlowLayout()
        layout.sectionInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        layout.itemSize = CGSize(width: collectionCellWidth, height: collectionCellHeight)
        layout.minimumInteritemSpacing = 0
        layout.minimumLineSpacing = 0
        layout.scrollDirection = .vertical
        
        colView = UICollectionView(frame: CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height), collectionViewLayout: layout)
        colView.delegate = self
        colView.dataSource = self
        colView.isScrollEnabled = true
        colView.register(CalendarCell.self, forCellWithReuseIdentifier: "calendarCell")
        colView.backgroundColor = UIColor.clear
        self.addSubview(colView)
    }
    
    //Calculate Days---------------
    func calculateDays()
    {
        
        //Calculating Leap year------------------------
        
        
        let curYear = vc.leaveView.curYearNum
        if (curYear % 4 == 0)
        {
            if (curYear % 100 == 0)
            {
                if (curYear % 400 == 0)
                {
                    monthDays[1] = 29
                }
                else
                {
                    monthDays[1] = 28
                }
            }
            else
            {
                monthDays[1] = 29
            }
        }
        else
        {
            monthDays[1] = 28
        }
        
        numOfDays = monthDays[vc.leaveView.curMonthIndex]
        
        //Calculating StartDay------------------------
        
        var now = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        now = dateFormatter2.date(from: "\(vc.leaveView.curYearNum)-\(vc.leaveView.curMonthNum)-1")!
        dateFormatter.string(from: now)
        //print("Today is \(dateFormatter.string(from: now))")
        
        switch(dateFormatter.string(from: now))
        {
        case "Monday" : startDay = 1
        case "Tuesday" : startDay = 2
        case "Wednesday" : startDay = 3
        case "Thursday" : startDay = 4
        case "Friday" : startDay = 5
        case "Saturday" : startDay = 6
        case "Sunday" : startDay = 0
        default : break
        }
        
    }

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return 49
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var isToday = false
        var leaveColor = UIColor.blue
        var isStopped = false
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "calendarCell", for: indexPath) as! CalendarCell
        
        if (indexPath.row < 7)
        {
            cell.initialize(vc : self.vc, lblStr: "\(days[indexPath.row])", today : false, leaveColor : UIColor.blue)
            cell.lbl.textColor = UIColor(red: 255/255, green: 46/255, blue: 46/255, alpha: 1)
        }
        else if (indexPath.row > 6 + startDay && indexPath.row - 6 - startDay <= monthDays[vc.leaveView.curMonthIndex] )
        {
            if(vc.leaveView.curMonthNum == vc.userInform.curMonthNum && indexPath.row - 6 - startDay == vc.userInform.curDayNum)
            {
                isToday = true
            }
            else
            {
                isToday = false
            }
            
            for var i in (0 ..< leaveDays.count)
            {
                if (Int(leaveDays[i])! == indexPath.row - 6 - startDay)
                {
                    print("\(Int(leaveDays[i])!) and \(indexPath.row - 6 - startDay)")
                    leaveColor = leaveColors[i]
                    isStopped = true
                }
                else if(!isStopped)
                {
                    leaveColor = UIColor.blue
                }
            }
            
            cell.initialize(vc : self.vc, lblStr: "\(indexPath.row - 6 - startDay)", today : isToday, leaveColor : leaveColor)
            isStopped = false
            
        }
        return cell
    }

}
