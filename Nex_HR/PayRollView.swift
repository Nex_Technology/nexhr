//
//  PayRollView.swift
//  Nex_HR
//
//  Created by Daud on 10/2/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class PayRollView: UIView, UIScrollViewDelegate {

    var sideAlignment : CGFloat = 20
    
    var vc : ViewController!
    
    var curMonth = "October"
    var curYear = "2016"
    var curYearNum = 0
    var curMonthNum = 0
    var curMonthIndex = 0
    var curYearIndex = 0
    var curDayNum = 0
    let title1 : [String] = ["Request Date","Department","Name","Basic Pay","Overtime Deduction","Overtime Payment","Bonus","Commision","Other Plus","Leave Deduction","Tax","Other Detach","Net Pay"]
    var results : [String] = ["","","","","","","","","","","","",""]
    var goBackState = "state1"
    var forceCancel = false
    var previousOffsetY : CGFloat = 0
    var autoGoBackReady = false
    
    let scrView = UIScrollView()
    let headerCover = UIView()
    let btnDatePick = UIButton()
    var lblResults = [UILabel]()
    let lblPayRoll = UIButton()
    
    func initialize(_ vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = UIColor.clear
        
        scrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        scrView.delegate = self
        scrView.delaysContentTouches = false
        scrView.showsVerticalScrollIndicator = false
        self.addSubview(scrView)
        
        lblPayRoll.setTitle("Payroll", for: .normal)
        lblPayRoll.setTitleColor(UIColor.black, for: .normal)
        lblPayRoll.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblPayRoll.sizeToFit()
        lblPayRoll.frame.origin.x = sideAlignment
        lblPayRoll.center.y = 50
        scrView.addSubview(lblPayRoll)
        
        let btnHome = UIButton(frame: CGRect(x: self.frame.width - sideAlignment - 35, y: sideAlignment , width: 35, height: 35))
        btnHome.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnHome.center.y = 50
        btnHome.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        scrView.addSubview(btnHome)
        
        let lblChooseDate = UILabel(frame : CGRect(x: sideAlignment, y: 130, width: 100, height: 100))
        lblChooseDate.text = "View Payroll for"
        lblChooseDate.textColor = UIColor.black
        lblChooseDate.font = UIFont(name: "SFUIText-Medium", size: 18)
        lblChooseDate.sizeToFit()
        scrView.addSubview(lblChooseDate)
        
        curMonthNum = vc.userInform.curMonthNum
        curYearNum = vc.userInform.curYearNum
        curMonth = vc.userInform.curMonth
        curYear = vc.userInform.curYear
        curMonthIndex = vc.userInform.curMonthIndex
        curYearIndex = vc.userInform.curYearIndex
        curDayNum = vc.userInform.curDayNum
        
        let lblCurDate = UILabel()
        lblCurDate.text = "Today: \(curDayNum).\(curMonth).\(curYear)"
        lblCurDate.textColor = UIColor.black
        lblCurDate.font = UIFont(name: "SFUIText-Regular", size: 16)
        lblCurDate.sizeToFit()
        lblCurDate.frame.origin.x = sideAlignment
        lblCurDate.center.y = 80
        lblCurDate.alpha = 0.6
        scrView.addSubview(lblCurDate)
        
        changeButtonDate()
        btnDatePick.setTitleColor(UIColor.white, for: .normal)
        btnDatePick.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 16)
        btnDatePick.backgroundColor = UIColor(red: 0/255, green: 175/255, blue: 255/255, alpha: 1)
        btnDatePick.layer.cornerRadius = 6
        btnDatePick.titleLabel?.textAlignment = .center
        btnDatePick.frame.origin.x = sideAlignment
        btnDatePick.frame.origin.y = lblChooseDate.frame.origin.y + lblChooseDate.frame.height + 8
        btnDatePick.addTarget(self, action: #selector(self.chooseDate(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
        scrView.addSubview(btnDatePick)
        
        headerCover.frame = CGRect(x: 0, y: -20, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 0
        self.addSubview(headerCover)
        
        let lblPayRoll2 = UILabel()
        lblPayRoll2.text = "PayRoll"
        lblPayRoll2.textColor = UIColor.black
        lblPayRoll2.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblPayRoll2.sizeToFit()
        lblPayRoll2.frame.origin.x = sideAlignment
        lblPayRoll2.center.y = headerCover.frame.height / 2
        headerCover.addSubview(lblPayRoll2)
        
        let btnHome2 = UIButton(frame: CGRect(x: self.frame.width - 35 - sideAlignment, y: sideAlignment , width: 35, height: 35))
        btnHome2.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome2.imageView?.contentMode = .scaleAspectFit
        btnHome2.center.y = headerCover.frame.height / 2
        btnHome2.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        headerCover.addSubview(btnHome2)
        
        for var i in (0 ..< 13)
        {
            let lbl = UILabel(frame : CGRect(x: sideAlignment, y: btnDatePick.frame.origin.y + btnDatePick.frame.height + 50.0 + 40.0 * CGFloat(i), width: 100, height: 100))
            lbl.text = title1[i]
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Medium", size: 16)
            lbl.sizeToFit()
            lbl.alpha = 0.7
            
            if (i == 12)
            {
                lbl.frame.origin.y = lbl.frame.origin.y + 30
                lbl.font = UIFont(name: "SFUIText-Bold", size: 20)
                lbl.sizeToFit()
                //lbl.alpha = 1
            }
            scrView.contentSize.height = lbl.frame.origin.y + lbl.frame.height + 50
            
            scrView.addSubview(lbl)
            
            let lblResult = UILabel(frame : CGRect(x: sideAlignment, y: btnDatePick.frame.origin.y + btnDatePick.frame.height + 50.0 + 40.0 * CGFloat(i), width: self.frame.width - sideAlignment * 2, height: 100))
            lblResult.text = results[i]
            lblResult.textColor = UIColor.black
            lblResult.font = UIFont(name: "SFUIText-Medium", size: 16)
            lblResult.textAlignment = .right
            lblResult.sizeToFit()
            lblResult.frame.origin.x = self.frame.width - sideAlignment - lblResult.frame.width
            lblResult.alpha = 0
            lblResults.append(lblResult)
            if (i == 12)
            {
                lblResult.font = UIFont(name: "SFUIText-Medium", size: 20)
                lblResult.frame.origin.y = lblResult.frame.origin.y + 30
            }
            scrView.addSubview(lblResult)
        }
        
        
    }
    
    //Change the date on the button and request the api
    func changeButtonDate()
    {
        btnDatePick.setTitle("\(curMonth) \(curYear)", for: .normal)
        btnDatePick.sizeToFit()
        btnDatePick.frame.size.width = btnDatePick.frame.width + 20
        btnDatePick.frame.size.height = btnDatePick.frame.height + 5
        api_Post()
    }
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func api_Post()
    {
        self.vc.loadingLabelUp(label: "Requesting..")
        let auth = vc.userInform.auth_token
        
        let parameters = [
            "employee_id" : "\(vc.userInform.id)",
            "month" : "\(curMonthNum)",
            "year" : "\(curYearNum)"
            ]
        
        Alamofire.request("\(BASE_URL)payrolls/detail_payroll?auth_token=\(auth)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.vc.loadingView.succeeded()
                    self.autoGoBackReady = true
                    self.resetProperties()
                    if let JSONtemp = JSON as? NSDictionary
                    {
                        self.setResults(JSON : JSON as! NSDictionary)
                    }
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    print("\(BASE_URL)payrolls/detail_payroll?auth_token=\(auth)")
                }
        }

    }
    
    func setResults(JSON : NSDictionary)
    {
        
        results[0] = JSON["payroll_date"] as! String
        results[1] = JSON["department"] as! String
        results[2] = vc.userInform.name
        
        let paySlipList = JSON["pay_slip_list"] as! [NSDictionary]
        
        var payJSON = paySlipList[0]
        var payJsonNum = payJSON["amount"] as? NSNumber
        results[3] = "\(payJsonNum!)"
        
        results[4] = "not sure"
        
        payJSON = paySlipList[1]
        payJsonNum = payJSON["amount"] as? NSNumber
        results[5] = "\(payJsonNum!)"
        
        payJSON = paySlipList[2]
        payJsonNum = payJSON["amount"] as? NSNumber
        results[6] = "\(payJsonNum!)"
        
        payJSON = paySlipList[3]
        payJsonNum = payJSON["amount"] as? NSNumber
        results[7] = "\(payJsonNum!)"
        
        results[8] = "not sure"
        
        payJSON = paySlipList[4]
        payJsonNum = payJSON["amount"] as? NSNumber
        results[9] = "\(payJsonNum!)"
        
        payJSON = paySlipList[3]
        payJsonNum = payJSON["amount"] as? NSNumber
        results[10] = "\(payJsonNum!)"
        
        results[11] = "not sure"
        
        payJsonNum = JSON["net_pay"] as? NSNumber
        results[12] = "\(payJsonNum!)"
        
        for var i in (0 ..< 13)
        {
            lblResults[i].text = results[i]
            lblResults[i].sizeToFit()
            lblResults[i].frame.origin.x = self.frame.width - sideAlignment - lblResults[i].frame.width
            lblResults[i].alpha = 1
        }
        
    }
    
    
    //resets some UI properties and variables to call the API again
    func resetProperties()
    {
        for var i in (0 ..< 13)
        {
            lblResults[i].alpha = 0
        }
    }
    
    
    //Go Back to Home Screen--------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func goBackHome(_ btn : UIButton)
    {
        vc.homeView.viewBtns[2].alpha = 1
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.vc.homeView.btnBorders[2].alpha = 1
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
        self.vc.homeView.comeBack(delay : 0.15)
    }
    
    
    //Choose Month and Year---------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func chooseDate(_ btn : UIButton)
    {
        vc.monthYearPicker = MonthYearPicker()
        vc.monthYearPicker.frame = vc.view.frame
        vc.monthYearPicker.initialize(vc, parentView : "PayRollView")
        vc.view.addSubview(vc.monthYearPicker)
    }
    
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrOffset = scrView.contentOffset.y
        
        if (scrOffset > previousOffsetY)
        {
            forceCancel = true
        }
        else if (scrOffset < previousOffsetY)
        {
            forceCancel = false
        }
        
        previousOffsetY = scrOffset
        
        if (scrOffset >= 110)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 1
                self.headerCover.frame.origin.y = 0
                
                }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 0
                self.headerCover.frame.origin.y = -20
                
            }, completion: nil)
        }
        
        if (scrOffset <= -0 && (goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            self.transform = CGAffineTransform(scaleX: 1 + ((scrOffset + 0) * 0.0015), y: 1 + ((scrOffset + 0) * 0.0015))
            self.center.y = self.vc.view.frame.height / 2 - (scrOffset + 0) * 0.5
        }
        else if((goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.center.y = self.vc.view.frame.height / 2
                
            }, completion: nil)
        }
        
        if (scrOffset <= -80 && goBackState == "state1" && autoGoBackReady)
        {
            goBackState = "state2"
            vc.viewSwitchLabel.showFromAbove(text: "release to go back")
        }
        else if (scrOffset >= -80 && goBackState == "state2" && autoGoBackReady)
        {
            goBackState = "state1"
            vc.viewSwitchLabel.hide()
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (goBackState == "state2" && !forceCancel && autoGoBackReady)
        {
            goBackState = "state3"
            vc.viewSwitchLabel.hide()
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.alpha = 0
                self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                self.frame.origin.y = self.vc.view.frame.height
                
            }, completion: {finish in
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.vc.homeView.viewBtns[2].alpha = 1
                })
                self.vc.homeView.comeBack(delay : 0)
                self.vc.homeView.btnBorders[2].alpha = 1
                self.removeFromSuperview()
                
            })
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        forceCancel = false
    }


}
