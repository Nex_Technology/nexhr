//
//  TimePicker.swift
//  Nex_HR
//
//  Created by Daud on 11/15/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class TimePicker: UIView, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {

    
    var vc : ViewController!
    var minuteCells = [PickerCell]()
    var hourCells = [PickerCell]()
    
    var pickerView = UIView()
    var hourTabView = UITableView()
    var minuteTabView = UITableView()
    let btnChoose = UIButton(type : .system)
    
    var curHourIndex = 0
    var curMinuteIndex = 0
    var curTimeIndex = 0
    var parentView = ""
    var cellNum = 0
    
    var sideAlignment : CGFloat = 20
    
    var hours = ["01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24"]
    var hoursGuide = ["1am","2am","3am","4am","5am","6am","7am","8am","9am","10am","11am","12pm","1pm","2pm","3pm","4pm","5pm","6pm","7pm","8pm","9pm","10pm","11pm","12am"]
    var minutes = [String]()
    
    func initialize(_ vc : ViewController, parentView : String, cellNum : Int)
    {
        self.vc = vc
        self.parentView = parentView
        self.cellNum = cellNum
        self.backgroundColor = UIColor.clear
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTapped(_:)))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
        if(parentView == "overtimeView")
        {
            curHourIndex = vc.overtimeFormView.curHourIndex[cellNum - 2]
            curMinuteIndex = vc.overtimeFormView.curMinuteIndex[cellNum - 2]
        }
        
        for var i in (00 ..< 61)
        {
            if (i < 10)
            {
                minutes.append("0\(i)")
            }
            else
            {
                minutes.append("\(i)")
            }
        }
        
        pickerView.frame = CGRect(x: self.frame.width / 2, y: self.frame.height, width: self.frame.width - (sideAlignment * 2), height: 450)
        pickerView.backgroundColor = UIColor.clear
        self.addSubview(pickerView)
        
        let lblChoose = UILabel(frame : CGRect(x: 0, y: 20, width: 0, height: 0))
        lblChoose.text = "Choose hour and minute"
        lblChoose.textAlignment = .center
        lblChoose.textColor = UIColor.black
        lblChoose.alpha = 0.4
        lblChoose.font = UIFont(name: "SFUIText-Regular", size: 18)
        lblChoose.sizeToFit()
        pickerView.addSubview(lblChoose)
        
        btnChoose.setTitle("Choose", for: .normal)
        btnChoose.setTitleColor(UIColor.black, for: .normal)
        btnChoose.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnChoose.sizeToFit()
        btnChoose.frame.origin.y = 350
        btnChoose.frame.origin.x = 0
        btnChoose.addTarget(self, action: #selector(self.confirmDate(_:)), for: .touchUpInside)
        btnChoose.tintColor = UIColor.white
        pickerView.addSubview(btnChoose)
        
        minuteTabView.frame = CGRect(x: pickerView.frame.width / 2, y: 70, width: pickerView.frame.width / 2, height: 250)
        minuteTabView.register(PickerCell.self, forCellReuseIdentifier: "minuteCell")
        minuteTabView.delegate = self
        minuteTabView.dataSource = self
        minuteTabView.separatorColor = UIColor.clear
        minuteTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(minuteTabView)
        
        hourTabView.frame = CGRect(x: 0, y: 70, width: pickerView.frame.width / 2, height: 250)
        hourTabView.register(PickerCell.self, forCellReuseIdentifier: "hourCell")
        hourTabView.delegate = self
        hourTabView.dataSource = self
        hourTabView.separatorColor = UIColor.clear
        hourTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(hourTabView)
        
        pickerView.transform = CGAffineTransform(scaleX: 0, y: 0)
        pickerView.alpha = 0
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.pickerView.center.y = self.vc.view.center.y
            
        }, completion: {finish in
            self.minuteTabView.scrollToRow(at: NSIndexPath(row: self.curMinuteIndex, section: 0) as IndexPath, at: .middle, animated: true)
            self.hourTabView.scrollToRow(at: NSIndexPath(row: self.curHourIndex, section: 0) as IndexPath, at: .middle, animated: true)
        }  )
        
        
        vc.overtimeView.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            
            self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            self.pickerView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.pickerView.center = CGPoint(x : self.frame.width / 2 , y : self.frame.height / 2)
            self.pickerView.alpha = 1
            
            self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            
        }, completion: nil)
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: {
            
        }, completion: {finish in
            
            self.minuteTabView.scrollToRow(at: NSIndexPath(row: self.curMinuteIndex, section: 0) as IndexPath, at: .middle, animated: true)
            self.hourTabView.scrollToRow(at: NSIndexPath(row: self.curHourIndex, section: 0) as IndexPath, at: .middle, animated: true)
            
        })
        
        
        
    }
    
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == minuteTabView)
        {
            return 61
        }
        else
        {
            return 24
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView == minuteTabView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "minuteCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize("\(minutes[indexPath.row]) min",tag : indexPath.row, color : curMinuteIndex == indexPath.row)
            minuteCells.append(cell)
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "hourCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize("\(hours[indexPath.row]) (\(hoursGuide[indexPath.row]))",tag : indexPath.row, color : curHourIndex == indexPath.row)
            hourCells.append(cell)
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // set current month and year when the table cells are tappped------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(tableView == minuteTabView)
        {
            for cc in minuteCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curMinuteIndex = indexPath.row
        }
        else if(tableView == hourTabView)
        {
            for cc in hourCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curHourIndex = indexPath.row
        }
    }
    
    //Choose Date-------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func confirmDate(_ btn : UIButton)
    {
        if(parentView == "overtimeView")
        {
            vc.overtimeFormView.curHourIndex[cellNum - 2] = curHourIndex
            vc.overtimeFormView.curMinuteIndex[cellNum - 2] = curMinuteIndex
            vc.overtimeFormView.curHour[cellNum - 2] = hours[curHourIndex]
            vc.overtimeFormView.curMinute[cellNum - 2] = minutes[curMinuteIndex]
            vc.overtimeFormView.tabView.reloadData()
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
            self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
            
        }, completion: {finish in
            
            self.removeFromSuperview()
            self.vc.overtimeView.alpha = 1
            
        })
        
    }
    
    //Cancle Picking----------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func backgroundTapped(_ gesture : UITapGestureRecognizer)
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
            self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
            
        }, completion: {finish in
            
            self.removeFromSuperview()
            self.vc.overtimeView.alpha = 1
            
        })
    }
    
    //Gesture View Delegate Methods-------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view != self && touch.view != pickerView && touch.view != hourTabView && touch.view != minuteTabView) {
            return false
        }
        else
        {
            return true
        }
    }
    


}
