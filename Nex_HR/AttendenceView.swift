//
//  Attendence.swift
//  Nex_HR
//
//  Created by Daud on 10/10/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class AttendenceView: UIView, UIScrollViewDelegate {

    var sideAlignment : CGFloat = 20
    let titles : [String] = ["Total Working Hour","Total Late Hour"]
    var results : [String] = ["",""]
    let cellWidths : [CGFloat] = [110,46,90,90,100]
    let cellStart : [CGFloat] = [0,110,170,260,350]
    let cellTitles : [String] = ["Date","Day","In","Out","Total Hours"]
    let cellDays : [String] = ["S","M","T","W","T","F","S"]
    let dayNames : [String] = ["Sun","Mon","Tue","Wed","Tur","Fri","Sat"]
    var goBackState = "state1"
    var forceCancel = false
    var previousOffsetY : CGFloat = 0
    var autoGoBackReady = false
    
    var weeks = [NSDictionary]()
    var days = [NSDictionary]()
    var lblDates = [UILabel]()
    var lblOffDays = [UILabel]()
    var lblDays = [UILabel]()
    var lblIns = [UILabel]()
    var lblOuts = [UILabel]()
    var lblTotalHours = [UILabel]()
    let scrView = UIScrollView()
    let headerCover = UIView()
    //let lblDate : [UILabel] = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    let btnDatePick = UIButton()
    var lblResults = [UILabel]()
    var btnWeeks = [UIButton]()
    var btnWeekSelector = UIView()
    let lblAttendence = UIButton()
    
    var vc : ViewController!
    
    var JSON : AnyObject!
    var curMonth = "October"
    var curYear = "2016"
    var curYearNum = 0
    var curMonthNum = 0
    var curMonthIndex = 0
    var curYearIndex = 0
    var curWeekNum = 0
    var curDayNum = 0
    
    
    func initialize(_ vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = UIColor.clear
        
        scrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        scrView.contentSize.height = 650
        scrView.delegate = self
        scrView.delaysContentTouches = false
        scrView.showsVerticalScrollIndicator = false
        self.addSubview(scrView)
        
        lblAttendence.setTitle("Attendance", for: .normal)
        lblAttendence.setTitleColor(UIColor.black, for: .normal)
        lblAttendence.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblAttendence.sizeToFit()
        lblAttendence.frame.origin.x = sideAlignment
        lblAttendence.center.y = 50
        scrView.addSubview(lblAttendence)
        
        let btnHome = UIButton(frame: CGRect(x: self.frame.width - sideAlignment - 35, y: sideAlignment , width: 35, height: 35))
        btnHome.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnHome.center.y = 50
        btnHome.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        scrView.addSubview(btnHome)
        
        headerCover.frame = CGRect(x: 0, y: -20, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 0
        self.addSubview(headerCover)
        
        let lblAttendence2 = UILabel()
        lblAttendence2.text = "Attendance"
        lblAttendence2.textColor = UIColor.black
        lblAttendence2.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblAttendence2.sizeToFit()
        lblAttendence2.frame.origin.x = sideAlignment
        lblAttendence2.center.y = headerCover.frame.height / 2
        headerCover.addSubview(lblAttendence2)
        
        let btnHome2 = UIButton(frame: CGRect(x: self.frame.width - 35 - sideAlignment, y: sideAlignment , width: 35, height: 35))
        btnHome2.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome2.imageView?.contentMode = .scaleAspectFit
        btnHome2.center.y = headerCover.frame.height / 2
        btnHome2.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        headerCover.addSubview(btnHome2)
        
        let lblChooseDate = UILabel(frame : CGRect(x: sideAlignment, y: 130, width: 100, height: 100))
        lblChooseDate.text = "View Attendence List for"
        lblChooseDate.textColor = UIColor.black
        lblChooseDate.font = UIFont(name: "SFUIText-Medium", size: 18)
        lblChooseDate.sizeToFit()
        scrView.addSubview(lblChooseDate)
        
        curMonthNum = vc.userInform.curMonthNum
        curYearNum = vc.userInform.curYearNum
        curMonth = vc.userInform.curMonth
        curYear = vc.userInform.curYear
        curMonthIndex = vc.userInform.curMonthIndex
        curYearIndex = vc.userInform.curYearIndex
        curDayNum = vc.userInform.curDayNum
        
        let lblCurDate = UILabel()
        lblCurDate.text = "Today: \(curDayNum).\(curMonth).\(curYear)"
        lblCurDate.textColor = UIColor.black
        lblCurDate.font = UIFont(name: "SFUIText-Regular", size: 16)
        lblCurDate.sizeToFit()
        lblCurDate.frame.origin.x = sideAlignment
        lblCurDate.center.y = 80
        lblCurDate.alpha = 0.6
        scrView.addSubview(lblCurDate)
        
        changeButtonDate()
        btnDatePick.setTitleColor(UIColor.white, for: .normal)
        btnDatePick.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 16)
        btnDatePick.backgroundColor = UIColor(red: 0/255, green: 175/255, blue: 255/255, alpha: 1)
        btnDatePick.layer.cornerRadius = 6
        btnDatePick.titleLabel?.textAlignment = .center
        btnDatePick.frame.origin.x = sideAlignment
        btnDatePick.frame.origin.y = lblChooseDate.frame.origin.y + lblChooseDate.frame.height + 8
        btnDatePick.addTarget(self, action: #selector(self.chooseDate(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
        scrView.addSubview(btnDatePick)
        
        for var i in (0 ..< 2)
        {
            let lbl = UILabel(frame : CGRect(x: sideAlignment, y: btnDatePick.frame.origin.y + btnDatePick.frame.height + 30.0 + 40.0 * CGFloat(i), width: 100, height: 100))
            lbl.text = titles[i]
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Medium", size: 16)
            lbl.sizeToFit()
            lbl.alpha = 0.7
            scrView.addSubview(lbl)
            
            let lblResult = UILabel(frame : CGRect(x: sideAlignment, y: btnDatePick.frame.origin.y + btnDatePick.frame.height + 30.0 + 40.0 * CGFloat(i), width: self.frame.width - sideAlignment * 2, height: 100))
            lblResult.text = ""
            lblResult.textColor = UIColor.black
            lblResult.font = UIFont(name: "SFUIText-Medium", size: 16)
            lblResult.textAlignment = .right
            lblResult.sizeToFit()
            lblResult.frame.origin.x = self.frame.width - sideAlignment - lblResult.frame.width
            lblResults.append(lblResult)
            scrView.addSubview(lblResult)
        }
        
        for var i in (0 ..< 5)
        {
            let btnWeek = UIButton(frame : CGRect(x: 0, y: 320, width: 100, height: 100))
            btnWeek.tag = i
            btnWeek.setTitle("week\(i+1)", for: .normal)
            btnWeek.setTitleColor(UIColor(red: 0, green: 188/255, blue: 196/255, alpha: 1), for: .normal)
            btnWeek.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 12)
            btnWeek.sizeToFit()
            btnWeek.center.x = ((self.frame.width) / 6) * CGFloat(i + 1)
            btnWeek.alpha = 0.4
            btnWeek.tag = i
            btnWeek.isUserInteractionEnabled = false
            btnWeek.addTarget(self, action: #selector(self.chooseWeek(_:)), for: .touchUpInside)
            btnWeeks.append(btnWeek)
            scrView.addSubview(btnWeek)
        }
        
        btnWeekSelector.frame = CGRect(x: 0, y: 0, width: 40, height: 30)
        btnWeekSelector.backgroundColor = UIColor(red: 0, green: 188/255, blue: 196/255, alpha: 1)
        btnWeekSelector.layer.cornerRadius = 4
        btnWeekSelector.center = btnWeeks[0].center
        btnWeekSelector.alpha = 0
        scrView.addSubview(btnWeekSelector)
        scrView.sendSubview(toBack: btnWeekSelector)
        
        setupTable()
        scrView.contentSize.height = 360 + 360
        if (scrView.contentSize.height < scrView.frame.height)
        {
            scrView.contentSize.height = scrView.frame.height + 1
        }
        
    }
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func api_Post()
    {
        let auth = vc.userInform.auth_token
        self.vc.loadingLabelUp(label: "Requesting..")
        
        let parameters = [
            "month" : "\(curMonthNum)",
            "year" : "\(curYearNum)"
        ]
        
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)/attendances?auth_token=\(auth)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.vc.loadingView.succeeded()
                    self.autoGoBackReady = true
                    self.JSON = JSON as! AnyObject
                    self.setResults(JSON : JSON as! NSDictionary, firstTime : true)
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                }
        }
        
    }
    
    func setResults(JSON : NSDictionary, firstTime : Bool)
    {
        resetProperties()
        
        if let twhNum = JSON["total_working_hour"] as? NSNumber
        {
            results[0] = "\(twhNum.doubleValue)"
        }
        if let tlhNum = JSON["total_late_hour"] as? NSNumber
        {
            results[1] = "\(tlhNum.doubleValue)"
        }
        
        for var i in (0 ..< 2)
        {
            lblResults[i].text = results[i]
            lblResults[i].sizeToFit()
            lblResults[i].frame.origin.x = self.frame.width - sideAlignment - lblResults[i].frame.width
        }
        
        weeks = JSON["weekly_attendance"] as! [NSDictionary]
        
        if weeks.count > 0
        {
            
            for var i in (0 ..< weeks.count)
            {
                btnWeeks[i].alpha = 1
                btnWeeks[i].isUserInteractionEnabled = true
            }
            
            if (firstTime)
            {
                UIView.animate(withDuration: 0.2, animations: {
                    
                    self.btnWeeks[0].setTitleColor(UIColor.white, for: .normal)
                    self.btnWeekSelector.center = self.btnWeeks[0].center
                    self.btnWeekSelector.alpha = 1
                    
                })
            }
            else
            {
                btnWeekSelector.alpha = 1
            }
            
            var days =  [NSDictionary]()
            days = weeks[curWeekNum]["attendance"] as! [NSDictionary]
            
            for var i in (0 ..< days.count)
            {
                let day = days[days.count - 1 - i]
                lblDates[i].text = day["date"] as! String
                lblDates[i].alpha = 0.7
                lblDates[i].sizeToFit()
                lblDates[i].frame.size.width = lblDates[i].frame.width + 2
                lblDays[i].text = sayDayName(date: day["date"] as! String)
                lblDays[i].alpha = 0.7
                lblDays[i].sizeToFit()
                lblDays[i].frame.size.width = lblDays[i].frame.width + 2
                lblIns[i].text = day["start_time"] as! String
                lblIns[i].alpha = 0.7
                lblIns[i].sizeToFit()
                lblIns[i].frame.size.width = lblIns[i].frame.width + 2
                lblOuts[i].text = day["end_time"] as! String
                lblOuts[i].alpha = 0.7
                lblOuts[i].sizeToFit()
                lblOuts[i].frame.size.width = lblOuts[i].frame.width + 2
                lblTotalHours[i].text = day["total_hour"] as! String
                lblTotalHours[i].alpha = 0.7
                lblTotalHours[i].sizeToFit()
                lblTotalHours[i].frame.size.width = lblTotalHours[i].frame.width + 2
                
                print("OFF DAY \(day["is_leave"] as! NSNumber)")
                
                if (day["is_leave"] as! NSNumber == 1)
                {
                    lblOffDays[i].alpha = 1
                }
                
                
            }
        }
        
        
    }
    
    //resets some UI properties and variables to call the API again
    func resetProperties()
    {
        for var i in (0 ..< 5)
        {
            btnWeeks[i].alpha = 0.4
            btnWeeks[i].isUserInteractionEnabled = false
        }
        for var i in (0 ..< 7)
        {
            lblDates[i].alpha = 0
            lblDays[i].alpha = 0
            lblIns[i].alpha = 0
            lblOuts[i].alpha = 0
            lblTotalHours[i].alpha = 0
            lblOffDays[i].alpha = 0
        }
        
        btnWeekSelector.alpha = 0
        for bb in btnWeeks
        {
            bb.setTitleColor(UIColor(red: 0, green: 188/255, blue: 196/255, alpha: 1), for: .normal)
        }
        
    }

    
    //Change the date on the button and request the api
    func changeButtonDate()
    {
        btnDatePick.setTitle("\(curMonth) \(curYear)", for: .normal)
        btnDatePick.sizeToFit()
        btnDatePick.frame.size.width = btnDatePick.frame.width + 20
        btnDatePick.frame.size.height = btnDatePick.frame.height + 5
        api_Post()
    }
    
    //return the name of the day from input date
    func sayDayName(date : String) -> String
    {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        if let todayDate = formatter.date(from: date)
        {
            let myCalendar = Calendar.current
            let weekDay = myCalendar.component(.weekday, from: todayDate)
            return dayNames[Int(weekDay)-1]
        }
        else
        {
            return ""
        }
    }
    
    //choose week from week buttons
    func chooseWeek(_ btn : UIButton)
    {
        curWeekNum = btn.tag
        setResults(JSON: self.JSON as! NSDictionary, firstTime : false)
        
        for bb in btnWeeks
        {
            bb.setTitleColor(UIColor(red: 0, green: 188/255, blue: 196/255, alpha: 1), for: .normal)
        }
        
        UIView.animate(withDuration: 0.2, animations: {
            
            self.btnWeekSelector.center = btn.center
            self.btnWeekSelector.alpha = 1
        
        }, completion:{ finish in
            btn.setTitleColor(UIColor.white, for: .normal)
        })
    }
    
    //Choose Month and Year---------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func chooseDate(_ btn : UIButton)
    {
        vc.monthYearPicker = MonthYearPicker()
        vc.monthYearPicker.frame = vc.view.frame
        vc.monthYearPicker.initialize(vc, parentView : "AttendanceView")
        vc.view.addSubview(vc.monthYearPicker)
    }
    
    
    //Setup Collection View---------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func setupTable()
    {
        let tableView = UIScrollView(frame : CGRect(x: 0, y: 360, width: self.frame.width, height: 360))
        let borderView = UIView(frame: CGRect(x: 0, y: 360, width: self.frame.width, height: 1))
        borderView.backgroundColor = UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1)
        scrView.addSubview(borderView)
        
        for var i in (0 ..< 5)
        {
            let lbl = UILabel(frame : CGRect(x: cellStart[i] + sideAlignment, y: 30, width: cellWidths[i], height: 80))
            lbl.text = cellTitles[i]
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Bold", size: 16)
            lbl.sizeToFit()
            tableView.addSubview(lbl)
            tableView.contentSize.width = lbl.frame.origin.x + lbl.frame.width + sideAlignment
        }
        for var i in (0 ..< 7)
        {
            let lbl = UILabel(frame : CGRect(x: cellStart[0] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[0], height: 80))
            lbl.text = "1.10.2015"
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl.sizeToFit()
            lbl.alpha = 0
            lblDates.append(lbl)
            tableView.addSubview(lbl)
            
            let lbl2 = UILabel(frame : CGRect(x: cellStart[1] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[1], height: 80))
            lbl2.text = cellDays[i]
            lbl2.textColor = UIColor.black
            lbl2.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl2.sizeToFit()
            lbl2.alpha = 0
            lblDays.append(lbl2)
            tableView.addSubview(lbl2)
            
            let lbl3 = UILabel(frame : CGRect(x: cellStart[2] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[2], height: 80))
            lbl3.text = "8:30 am"
            lbl3.textColor = UIColor.black
            lbl3.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl3.sizeToFit()
            lbl3.alpha = 0
            lblIns.append(lbl3)
            tableView.addSubview(lbl3)
            
            let lbl4 = UILabel(frame : CGRect(x: cellStart[3] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[3], height: 80))
            lbl4.text = "6:30 pm"
            lbl4.textColor = UIColor.black
            lbl4.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl4.sizeToFit()
            lbl4.alpha = 0
            lblOuts.append(lbl4)
            tableView.addSubview(lbl4)
            
            let lbl5 = UILabel(frame : CGRect(x: cellStart[4] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[4], height: 80))
            lbl5.text = "6:30 pm"
            lbl5.textColor = UIColor.black
            lbl5.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl5.sizeToFit()
            lbl5.alpha = 0
            lblTotalHours.append(lbl5)
            tableView.addSubview(lbl5)
            
            let lblOffDay = UILabel(frame : CGRect(x: cellStart[2] + sideAlignment, y: 40 * CGFloat(i) + 73, width: (cellStart[4] + cellWidths[4]) - cellStart[2], height: 30))
            lblOffDay.backgroundColor = UIColor(red : 0/255, green : 204/255, blue : 126/255, alpha : 1)
            lblOffDay.layer.cornerRadius = 5
            lblOffDay.textColor = UIColor.white
            lblOffDay.text = "Off Day"
            lblOffDay.textAlignment = .center
            lblOffDay.font = UIFont(name: "SFUIText-Medium", size: 15)
            lblOffDay.clipsToBounds = true
            lblOffDay.alpha = 0
            lblOffDays.append(lblOffDay)
            tableView.addSubview(lblOffDay)
            
        }
        
        
        //tableView.layer.addBorder(.top, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        scrView.addSubview(tableView)
    }
    
    //Go Back to Home Screen--------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func goBackHome(_ btn : UIButton)
    {
        vc.homeView.viewBtns[0].alpha = 1
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.vc.homeView.btnBorders[0].alpha = 1
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
        self.vc.homeView.comeBack(delay : 0.15)
    }
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrOffset = scrView.contentOffset.y
        
        if (scrOffset > previousOffsetY)
        {
            forceCancel = true
        }
        else if (scrOffset < previousOffsetY)
        {
            forceCancel = false
        }
        
        previousOffsetY = scrOffset
        
        if (scrOffset >= 110)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 1
                self.headerCover.frame.origin.y = 0
                
            }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 0
                self.headerCover.frame.origin.y = -20
                
            }, completion: nil)
        }
        
        if (scrOffset <= -0 && (goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            self.transform = CGAffineTransform(scaleX: 1 + ((scrOffset + 0) * 0.0015), y: 1 + ((scrOffset + 0) * 0.0015))
            self.center.y = self.vc.view.frame.height / 2 - (scrOffset + 0) * 0.5
        }
        else if((goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.center.y = self.vc.view.frame.height / 2
                
            }, completion: nil)
        }
        
        if (scrOffset <= -80 && goBackState == "state1" && autoGoBackReady)
        {
            goBackState = "state2"
            vc.viewSwitchLabel.showFromAbove(text: "Release to go back")
        }
        else if (scrOffset >= -80 && goBackState == "state2" && autoGoBackReady)
        {
            goBackState = "state1"
            vc.viewSwitchLabel.hide()
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (goBackState == "state2" && !forceCancel && autoGoBackReady)
        {
            goBackState = "state3"
            vc.viewSwitchLabel.hide()
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.alpha = 0
                self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                self.frame.origin.y = self.vc.view.frame.height
                
            }, completion: {finish in
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.vc.homeView.viewBtns[0].alpha = 1
                })
                self.vc.homeView.comeBack(delay : 0)
                self.vc.homeView.btnBorders[0].alpha = 1
                self.removeFromSuperview()
                
            })
        }
    }


}
