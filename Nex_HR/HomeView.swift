//
//  HomeView.swift
//  Nex_HR
//
//  Created by Daud on 9/28/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class HomeView: UIView, UIScrollViewDelegate, UITextFieldDelegate {

    var name = "Zayer Zy"
    var email = "Zaya@nexlabs.co"
    let sideAlignment : CGFloat = 20
    
    var bgColor2 = UIColor(red: 57/255, green: 62/255, blue: 68/255, alpha: 1)
    let bgColor = UIColor.clear
    
    let lblName = UILabel()
    let lblEmail = UILabel()
    let btnSettings = UIButton()
    let btnProfile = UIButton()
    let view2 = UIView()
    let scrView = UIScrollView()
    let coverPhoto = UIImageView(image: UIImage(named: "coverProfile.jpg"))
//    let btnAttendance = UIButton()
//    let btnLeave = UIButton()
//    let btnPayroll = UIButton()
//    let btnOvertime = UIButton()
//    let btnHandBook = UIButton()
    var viewBtns = [UIButton(),UIButton(),UIButton(),UIButton(),UIButton()]
    var btnBorders = [UIView(),UIView(),UIView(),UIView(),UIView()]
    var btnLabels = ["Attendance","Leave","Payroll","Overtime","Handbook"]
    var viewBtnOffset = [CGFloat(),CGFloat(),CGFloat(),CGFloat(),CGFloat()]
    var btnBorderOffset = [CGFloat(),CGFloat(),CGFloat(),CGFloat(),CGFloat()]
    let btnCheckIn = UIButton(type : .system)
    let imgArrow = UIImageView(image : UIImage(named : "btnArrow.png"))
    let settingView = UIScrollView()
    let lblSetting = UILabel()
    let btnLogout = UIButton(type: .system)
    let btnChangePassword = UIButton(type: .system)
    let btnCancel = UIButton(type: .system)
    
    let txtPassword = UITextField()
    let lblPassword = UILabel()
    let lblCondition = UILabel()
    let lblCredit = UILabel()
    let btnNext = UIButton(type : .system)
    var passStage = 0
    var newPassword = ""
    
    var goBackState = "state1"
    var forceCancel = false
    var previousOffsetY : CGFloat = 0
    var autoGoBackReady = true
    
    var changingPassword = false
    
    var vc : ViewController!
    
    func initialize(_ vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = bgColor
        
        scrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        scrView.delegate = self
//        scrView.delaysContentTouches = false
        self.addSubview(scrView)
        
        coverPhoto.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 240)
        coverPhoto.image = UIImage(named: "coverProfile.jpg")
        coverPhoto.contentMode = .scaleAspectFill
        coverPhoto.clipsToBounds = true
        coverPhoto.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.profileTapped(_ :))))
        coverPhoto.isUserInteractionEnabled = true
        scrView.addSubview(coverPhoto)
        
        let borderLine = UIView(frame : CGRect(x: 0, y: coverPhoto.frame.height, width: self.frame.width, height: 1))
        borderLine.backgroundColor = UIColor(red: 240/255, green : 240/255, blue : 240/255, alpha : 1)
//        scrView.addSubview(borderLine)
        
        lblName.frame = CGRect(x: sideAlignment, y: coverPhoto.frame.height - 75, width: 100, height: 20)
        lblEmail.frame = CGRect(x: sideAlignment, y: coverPhoto.frame.height - 35, width: 100, height: 20)
        lblName.text = name
        lblEmail.text = email
        lblName.textColor = UIColor.black
        lblEmail.textColor = UIColor.black
        lblName.font = UIFont(name: "SFUIText-Bold", size: 32)
        lblEmail.font = UIFont(name: "SFUIText-Medium", size: 20)
        lblName.sizeToFit()
        lblEmail.sizeToFit()
        lblName.alpha = 1
        lblName.layer.shadowRadius = 10
        lblName.layer.shadowOpacity = 0.2
        lblEmail.alpha = 1
        lblEmail.layer.shadowRadius = 10
        lblEmail.layer.shadowOpacity = 0.2
        scrView.addSubview(lblName)
        scrView.addSubview(lblEmail)
        
        imgArrow.frame = CGRect(x: 0, y: 0, width: 10, height: 10)
        imgArrow.frame.origin.x = lblEmail.frame.width + 10
        imgArrow.center.y = lblEmail.frame.height - 10
        lblEmail.addSubview(imgArrow)

        btnSettings.backgroundColor = UIColor.clear
        btnSettings.setImage( UIImage(named: "btnSettings.png"), for: .normal)
        btnSettings.frame = CGRect(x: self.frame.width - 35 - sideAlignment, y: sideAlignment - 5, width: 35, height: 35)
        btnSettings.layer.cornerRadius = 5
        btnSettings.center.y = 50
        btnSettings.addTarget(self, action: #selector(self.showSettingView(_:)), for: .touchUpInside)
        scrView.addSubview(btnSettings)
        
        btnCheckIn.setTitle("Check In", for: .normal)
        btnCheckIn.setTitleColor(UIColor.white, for: .normal)
        btnCheckIn.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 16)
        btnCheckIn.sizeToFit()
        btnCheckIn.backgroundColor = UIColor(red: 0, green: 175/255, blue: 255/255, alpha: 1)
        btnCheckIn.frame = CGRect(x: btnSettings.frame.origin.x - btnCheckIn.frame.width - 20 - 15, y: sideAlignment, width: btnCheckIn.frame.width + 20, height: 35)
        btnCheckIn.layer.cornerRadius = 5
        btnCheckIn.center.y = 50
        btnCheckIn.tintColor = UIColor.white
        btnCheckIn.addTarget(self, action: #selector(self.checkIn(_:)), for: .touchUpInside)
        scrView.addSubview(btnCheckIn)
        
        for var i in( 0 ..< 5)
        {
            let btn = UIButton(type : .system)
            btn.frame = CGRect(x: sideAlignment, y: coverPhoto.frame.height + 30 + CGFloat(i * 65), width: self.frame.width - sideAlignment, height: 20)
            btn.contentHorizontalAlignment = .left
//            btn.backgroundColor = UIColor.red
            btn.setTitle(btnLabels[i], for: .normal)
            btn.setTitleColor(UIColor.black, for: .normal)
            btn.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
            btn.sizeToFit()
            btn.frame.size.width = self.frame.width - sideAlignment
            viewBtnOffset[i] = btn.frame.origin.y
            scrView.addSubview(btn)
            viewBtns[i] = btn
            btn.tintColor = UIColor.white
            
            scrView.contentSize.height = btn.frame.origin.y + btn.frame.height + 80
            if (scrView.contentSize.height < scrView.frame.height)
            {
                scrView.contentSize.height = scrView.frame.height + 1
            }
            
            if (i == 0)
            {
                btn.addTarget(self, action: #selector(self.attendenceTapped(_:)), for: .touchUpInside)
            }
            else if (i == 1)
            {
                btn.addTarget(self, action: #selector(self.leaveTapped(_:)), for: .touchUpInside)
                btn.alpha = 0.5
                btn.isUserInteractionEnabled = false
            }
            else if (i == 2)
            {
                btn.addTarget(self, action: #selector(self.payRollTapped(_:)), for: .touchUpInside)
            }
            else if (i == 3)
            {
                btn.addTarget(self, action: #selector(self.overtimeTapped(_:)), for: .touchUpInside)
            }
            else if (i == 4)
            {
                btn.addTarget(self, action: #selector(self.employeeTapped(_:)), for: .touchUpInside)
            }
            
            let borderLine = UIView(frame : CGRect(x: sideAlignment, y: btn.frame.origin.y + 45, width: self.frame.width - sideAlignment, height: 1))
            borderLine.backgroundColor = UIColor(red: 240/255, green : 240/255, blue : 240/255, alpha : 1)
            btnBorders[i] = borderLine
            btnBorderOffset[i] = borderLine.frame.origin.y
            scrView.addSubview(borderLine)
            
        }
        
        setupSettingView()
        print ("Device Unique ID is : \(UIDevice.current.identifierForVendor!.uuidString)")
        
    }
    
    //Check In--------------------------------------------------------------
    func checkIn(_ btn : UIButton)
    {
        vc.checkIn = CheckIn()
        vc.checkIn.frame = self.frame
        vc.checkIn.initialize(vc: self.vc)
        vc.view.addSubview(vc.checkIn)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            
            self.transform = CGAffineTransform(scaleX: 0.9, y: 0.9)
            self.alpha = 0
            
        }, completion: nil)
        
        
    }
    
    //Setup Setting View----------------------------------------------------
    func setupSettingView()
    {
        settingView.frame = CGRect(x: 0, y: self.frame.height, width: self.frame.width, height: self.frame.height)
        settingView.backgroundColor = UIColor.clear
        settingView.alpha = 0
        settingView.contentSize.height = settingView.frame.height + 1
        settingView.delegate = self
        self.addSubview(settingView)
        
        lblSetting.text = "Settings"
        lblSetting.textColor = UIColor.black
        lblSetting.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblSetting.sizeToFit()
        lblSetting.frame.origin.x = sideAlignment
        lblSetting.center.y = 50
        settingView.addSubview(lblSetting)
        
        btnCancel.setTitle("Cancel", for: .normal)
        btnCancel.setTitleColor(UIColor.black, for: .normal)
        btnCancel.titleLabel?.textAlignment = .center
        btnCancel.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 18)
        btnCancel.sizeToFit()
        btnCancel.center.y = lblSetting.center.y
        btnCancel.frame.origin.x = self.frame.width - btnCancel.frame.width - sideAlignment
        btnCancel.addTarget(self, action: #selector(self.hideSettingView(_:)), for: .touchUpInside)
        btnCancel.tintColor = UIColor.white
        settingView.addSubview(btnCancel)
        
        btnLogout.setTitle("Log Out", for: .normal)
        btnLogout.setTitleColor(UIColor.white, for: .normal)
        btnLogout.titleLabel?.textAlignment = .center
        btnLogout.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnLogout.backgroundColor = UIColor(red: 255/255, green: 76/255, blue: 64/255, alpha: 1)
        btnLogout.sizeToFit()
        btnLogout.layer.cornerRadius = 6
        btnLogout.frame = CGRect(x: sideAlignment, y: 120, width: btnLogout.frame.width + 30, height: btnLogout.frame.height + 5)
        btnLogout.addTarget(self, action: #selector(self.logOut(_:)), for: .touchUpInside)
        btnLogout.tintColor = UIColor.white
        settingView.addSubview(btnLogout)
        
        btnChangePassword.setTitle("Change Password", for: .normal)
        btnChangePassword.setTitleColor(UIColor.black, for: .normal)
        btnChangePassword.titleLabel?.textAlignment = .center
        btnChangePassword.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 22)
        btnChangePassword.backgroundColor = UIColor(red: 242/255, green: 242/255, blue: 242/255, alpha: 1)
        btnChangePassword.sizeToFit()
        btnChangePassword.layer.cornerRadius = 6
        btnChangePassword.frame = CGRect(x: sideAlignment, y: btnLogout.frame.origin.y + btnLogout.frame.height + 10, width: btnChangePassword.frame.width + 30, height: btnChangePassword.frame.height + 5)
        btnChangePassword.addTarget(self, action: #selector(self.changePassword(_:)), for: .touchUpInside)
        btnChangePassword.tintColor = UIColor.white
        settingView.addSubview(btnChangePassword)
        
        lblPassword.text = "Type old password"
        lblPassword.frame = CGRect(x: sideAlignment, y: 100, width: self.frame.width - sideAlignment * 2, height: 30)
        lblPassword.font = UIFont(name: "SFUIText-Bold", size: 20)
        lblPassword.textColor = UIColor.black
        lblPassword.alpha = 0
        settingView.addSubview(lblPassword)
        
        txtPassword.text = ""
        txtPassword.frame = CGRect(x: sideAlignment, y: 160, width: self.frame.width - sideAlignment * 2, height: 30)
        txtPassword.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        txtPassword.font = UIFont(name: "SFUIText-Bold", size: 25)
        txtPassword.textColor = UIColor.black
        txtPassword.isUserInteractionEnabled = true
        txtPassword.isSecureTextEntry = true
        txtPassword.returnKeyType = .next
        txtPassword.delegate = self
        txtPassword.alpha = 0
        settingView.addSubview(txtPassword)
        
        lblCondition.text = ""
        lblCondition.frame = CGRect(x: sideAlignment, y: 200, width: self.frame.width - sideAlignment * 2, height: 30)
        lblCondition.font = UIFont(name: "SFUIText-Bold", size: 18)
        lblCondition.textColor = UIColor(red: 255/255, green: 76/255, blue: 64/255, alpha: 1)
        lblCondition.alpha = 0
        settingView.addSubview(lblCondition)
        
        btnNext.setTitle("Next", for: .normal)
        btnNext.setTitleColor(UIColor.black, for: .normal)
        btnNext.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnNext.sizeToFit()
        btnNext.frame.origin.y = 220
        btnNext.frame.origin.x = self.frame.width - btnNext.frame.width - sideAlignment
        btnNext.alpha = 0
        btnNext.addTarget(self, action: #selector(self.nextStep(_:)), for: .touchUpInside)
        settingView.addSubview(btnNext)
        
        
        lblCredit.font = UIFont(name: "SFUIText-Bold", size: 14)
        lblCredit.textColor = UIColor(red: 0/255, green: 0/255, blue: 0/255, alpha: 0.2)
        settingView.addSubview(lblCredit)
        
        
    }
    
    //Settings-------------------------------------------------------------------------------
    
    func showSettingView(_ btn : UIButton)
    {
        settingView.alpha = 1
        lblPassword.text = "Type old password"
        
        let rand = Int(arc4random_uniform(8))
        
        if (rand == 0)
        {
            lblCredit.text = "For beloved Nex rockstars."
        }
        else if (rand == 1)
        {
            lblCredit.text = "Tip 101 : Always try to be cool."
        }
        else if (rand == 2)
        {
            lblCredit.text = "Don't even think about using office wifi."
        }
        else if (rand == 3)
        {
            lblCredit.text = "Be a happy Nexer."
        }
        else if (rand == 4)
        {
            lblCredit.text = "\\m/"
        }
        else if (rand == 5)
        {
            lblCredit.text = "Celebrate your birthday at office."
        }
        else if (rand == 6)
        {
            lblCredit.text = "Being angry makes you ugly."
        }
        else if (rand == 7)
        {
            lblCredit.text = "Don't download Torrent at office."
        }
        
        lblCredit.sizeToFit()
        lblCredit.center = CGPoint(x: settingView.frame.width / 2 , y: settingView.frame.height - 50)
        
        UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
            
            self.frame.origin.y = -self.frame.height
            self.frame.size.height = self.frame.height * 2
            
        }, completion: nil)

    }
    
    func hideSettingView(_ btn : UIButton)
    {
        if (changingPassword)
        {
            self.endEditing(true)
            changingPassword = false
            UIView.animate(withDuration: 0.3, animations: {
                self.btnLogout.alpha = 1
                self.btnChangePassword.alpha = 1
                self.lblSetting.alpha = 1
                self.txtPassword.alpha = 0
                self.lblPassword.alpha = 0
                self.lblCondition.alpha = 0
                self.btnNext.alpha = 0
            })
        }
        else
        {
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
                
                self.frame.origin.y = 0
                self.frame.size.height = self.frame.height / 2
                
            }, completion: {finish in
                
                self.settingView.alpha = 0
                
            })
            
        }
        
    }
    
    func logOut(_ btn : UIButton)
    {
        
        vc.loadingLabelUp(label : "Logging Out..")
        Alamofire.request("\(BASE_URL)employees/logout?auth_token=\(vc.userInform.auth_token)", method: .delete, parameters: nil, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    
                    self.vc.loadingView.succeeded()
                    let userDefault = UserDefaults.standard
                    userDefault.set(false, forKey: "signedIn")
                    self.vc.checkSignedIn()
                    self.settingView.alpha = 0
                    self.removeFromSuperview()
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                }
        }
        
    }
    
    func changePassword(_ btn : UIButton)
    {
        self.passStage = 0
        changingPassword = true
        txtPassword.becomeFirstResponder()
        
        btnNext.alpha = 1
        btnNext.isUserInteractionEnabled = true
        btnNext.setTitleColor(UIColor.black, for: .normal)
        btnNext.setTitle("Next", for: .normal)
        txtPassword.text = ""
        lblPassword.text = "Type old password"
        
        UIView.animate(withDuration: 0.3, animations: {
            self.btnLogout.alpha = 0
            self.btnChangePassword.alpha = 0
            self.lblSetting.alpha = 0
            self.txtPassword.alpha = 1
            self.lblPassword.alpha = 1
            self.btnNext.alpha = 1
        })
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        nextStep(UIButton())
        return true
    }
    
    func nextStep(_ btn : UIButton)
    {
        let userDefault = UserDefaults.standard
        lblCondition.alpha = 0

        if (passStage == 0)
        {
            if (txtPassword.text == userDefault.string(forKey: "password"))
            {
                passStage = 1
                self.lblPassword.text = "Type new password"
                
                UIView.animate(withDuration: 0.2, animations: {
                    
                    self.txtPassword.alpha = 0
                    self.lblCondition.alpha = 0
                    self.lblPassword.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    
                },completion : { finish in
                    
                    self.txtPassword.text = ""
                    UIView.animate(withDuration: 0.2, animations: {
                        
                        self.txtPassword.alpha = 1
                        self.lblPassword.transform = CGAffineTransform(scaleX: 1, y: 1)
                        
                    })
                })
                
            }
            else
            {
                lblCondition.text = "wrong password"
                lblCondition.alpha = 1
            }
        }
        else if (passStage == 1)
        {
            if ((txtPassword.text?.characters.count)! >= 8)
            {
                newPassword = txtPassword.text!
                self.lblPassword.text = "Confirm password"
                passStage = 2
                btnNext.setTitle("Change", for: .normal)
                btnNext.sizeToFit()
                btnNext.setTitleColor(UIColor(red: 18 / 255 , green : 158 / 255 , blue : 229 / 255, alpha : 1), for: .normal)
                btnNext.frame.origin.x = self.frame.width - btnNext.frame.width - sideAlignment
                txtPassword.returnKeyType = .done
                print("Step 2 succeeded")
                
                UIView.animate(withDuration: 0.2, animations: {
                    
                    self.txtPassword.alpha = 0
                    self.lblPassword.transform = CGAffineTransform(scaleX: 1.2, y: 1.2)
                    
                },completion : { finish in
                    
                    self.txtPassword.text = ""
                    UIView.animate(withDuration: 0.2, animations: {
                        
                        self.txtPassword.alpha = 1
                        self.lblPassword.transform = CGAffineTransform(scaleX: 1, y: 1)
                        
                    })
                })
            }
            else
            {
                lblCondition.text = "Must have at least 8 characters"
                lblCondition.alpha = 1
            }
        }
        else if (passStage == 2)
        {
            if (newPassword == txtPassword.text!)
            {
                print("password changed")
                self.endEditing(true)
                lblCondition.alpha = 0
                api_changePassword()
            }
            else
            {
                lblCondition.text = "different from before"
                lblCondition.alpha = 1
            }
            
            
        }
    }
    
    func api_changePassword()
    {
        
        btnNext.alpha = 0.4
        btnNext.isUserInteractionEnabled = false
        
        vc.loadingLabelUp(label : "Changing Password..")
        let userDefault = UserDefaults.standard
        
        let parameters = [
            "old_password" : "\(userDefault.string(forKey: "password")!)",
            "password" : "\(newPassword)",
            "password_confirmation" : "\(newPassword)"
        ]
        print(parameters)
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)/change_password?auth_token=\(vc.userInform.auth_token)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    
                    self.vc.loadingView.succeeded()
                    userDefault.set("\(self.newPassword)", forKey : "password")
                    self.hideSettingView(UIButton())
                    print(JSON)
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                    self.btnNext.alpha = 1
                    self.btnNext.isUserInteractionEnabled = true
                    self.vc.loadingView.failed()
                }
        }
    }
    
    //Profile Button Tapped-------------------------------------------------
    func profileTapped(_ gesture : UITapGestureRecognizer)
    {
        
        self.vc.profileView = ProfileView()
        self.vc.profileView.frame = self.vc.view.frame
        self.vc.view.addSubview(self.vc.profileView)
        self.vc.profileView.initialize(self.vc, btnProfileY: self.btnProfile.frame.origin.y)
        lblName.alpha = 0
        lblEmail.alpha = 0
        coverPhoto.alpha = 0
        
        UIView.animate(withDuration: 0.2, delay: 0, options: .curveEaseOut, animations: {
            
            self.scrView.contentOffset.y = 0
            self.transform = CGAffineTransform(scaleX: 0.95, y: 0.95)
            self.alpha = 0
            
        }, completion: { finish in
        })
        
    }
    
    //Attendence Button Tapped----------------------------------------------
    func attendenceTapped(_ btn : UIButton)
    {
        self.vc.attendenceView = AttendenceView()
        self.vc.view.addSubview(self.vc.attendenceView)
        self.vc.attendenceView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.vc.attendenceView.initialize(self.vc)
        vc.view.isUserInteractionEnabled = false
        leaveAnim(comingView: vc.attendenceView, viewNum : 0, lbl : vc.attendenceView.lblAttendence)
        
    }
    
    //Leave View Come Up----------------------------------------------------
    func leaveTapped(_ btn : UIButton)
    {
        self.vc.leaveView = LeaveView()
        self.vc.view.addSubview(self.vc.leaveView)
        self.vc.leaveView.frame = self.vc.view.frame
        self.vc.leaveView.initialize(self.vc)
        vc.view.isUserInteractionEnabled = false
        leaveAnim(comingView: vc.leaveView, viewNum : 1, lbl : vc.leaveView.lblLeave)
        
    }
    
    //Leave Button Tapped----------------------------------------------
    func overtimeTapped(_ btn : UIButton)
    {
        self.vc.overtimeView = OvertimeView()
        self.vc.view.addSubview(self.vc.overtimeView)
        self.vc.overtimeView.frame = self.vc.view.frame
        self.vc.overtimeView.initialize(self.vc)
        vc.view.isUserInteractionEnabled = false
        leaveAnim(comingView: vc.overtimeView, viewNum : 3, lbl : vc.overtimeView.lblOvertime)
        
    }
    
    //PayRoll View Come Up--------------------------------------------------
    func payRollTapped(_ btn : UIButton)
    {
        self.vc.payRollView = PayRollView()
        self.vc.view.addSubview(self.vc.payRollView)
        self.vc.payRollView.frame = self.vc.view.frame
        self.vc.payRollView.initialize(self.vc)
        vc.view.isUserInteractionEnabled = false
        leaveAnim(comingView: vc.payRollView, viewNum : 2, lbl : vc.payRollView.lblPayRoll)
        
    }
    
    //EmployeeHandbook View Come Up--------------------------------------------------
    func employeeTapped(_ btn : UIButton)
    {
        
        self.vc.employeeMenu = EmployeeMenu()
        self.vc.view.addSubview(self.vc.employeeMenu)
        self.vc.employeeMenu.frame = self.vc.view.frame
        self.vc.employeeMenu.initialize(self.vc)
        vc.view.isUserInteractionEnabled = false
        leaveAnim(comingView: vc.employeeMenu, viewNum : 4, lbl : vc.employeeMenu.lblEmployee)
        
//        vc.statusBarHidden = true
        
//        self.vc.employeeHandBook = EmployeeHandbook()
//        self.vc.view.addSubview(self.vc.employeeHandBook)
//        self.vc.employeeHandBook.frame = self.vc.view.frame
//        self.vc.employeeHandBook.frame.origin.y = self.vc.view.frame.height
//        self.vc.employeeHandBook.initialize(vc: vc)
//        
//        UIView.animate(withDuration: 0.1, animations: {
//            
//            for cc in self.btnBorders
//            {
//                cc.alpha = 0
//            }
//            
//        })
//        
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
//            
//            self.alpha = 0
//            self.vc.view.backgroundColor = UIColor.black
//            self.frame.origin.y = -self.vc.view.frame.height
//            self.vc.employeeHandBook.frame.origin.y = 0
//            
//        },completion : { finish in
//            
//            self.vc.employeeHandBook.getPDFLink()
//            
//        })
    }
    
    //Leave animation to other views-----------------------------------------
    func leaveAnim(comingView : UIView, viewNum : Int, lbl : UIButton)
    {
        comingView.alpha = 0
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            self.scrView.contentOffset.y = 0
            for var i in ( 0 ..< 5)
            {
                if (viewNum == i)
                {
                    self.viewBtns[i].center.y = 50
                    self.btnBorders[i].frame.origin.y = self.viewBtns[i].frame.origin.y + 40
                    self.btnBorders[i].alpha = 0
                }
                else if (i < viewNum)
                {
                    self.viewBtns[i].frame.origin.y = self.viewBtns[i].frame.origin.y - self.frame.height
                    self.btnBorders[i].frame.origin.y = self.btnBorders[i].frame.origin.y - self.frame.height
                }
                else
                {
                    self.viewBtns[i].frame.origin.y = self.viewBtns[i].frame.origin.y + self.frame.height
                    self.btnBorders[i].frame.origin.y = self.btnBorders[i].frame.origin.y + self.frame.height
                }
            }
            self.coverPhoto.frame.origin.y = self.coverPhoto.frame.origin.y - self.frame.height
            self.lblName.frame.origin.y = self.lblName.frame.origin.y - self.frame.height
            self.lblEmail.frame.origin.y = self.lblEmail.frame.origin.y - self.frame.height
            self.btnCheckIn.frame.origin.y = self.btnCheckIn.frame.origin.y - self.frame.height
            self.btnSettings.frame.origin.y = self.btnSettings.frame.origin.y - self.frame.height
            
        }, completion: {finish in
            
            UIView.animate(withDuration: 0.2, delay: 0, options: [], animations: {
                
                comingView.alpha = 1
                
                
            }, completion: {finish in
                
                self.vc.view.isUserInteractionEnabled = true
                lbl.alpha = 1
                self.viewBtns[viewNum].alpha = 0
                
            })
        })
        
    }
    
    
    //Come back animation from other views--------------------------------------------
    func comeBack(delay : CGFloat)
    {
        UIView.animate(withDuration: 0.3, delay: TimeInterval(delay), options: .curveEaseOut, animations: {
            self.coverPhoto.frame.origin.y = self.coverPhoto.frame.origin.y + self.frame.height
            self.lblName.frame.origin.y = self.lblName.frame.origin.y + self.frame.height
            self.lblEmail.frame.origin.y = self.lblEmail.frame.origin.y + self.frame.height
            self.btnCheckIn.frame.origin.y = self.btnCheckIn.frame.origin.y + self.frame.height
            self.btnSettings.frame.origin.y = self.btnSettings.frame.origin.y + self.frame.height
            
            for var i in ( 0 ..< 5)
            {
                self.viewBtns[i].frame.origin.y = self.viewBtnOffset[i]
                self.btnBorders[i].frame.origin.y = self.btnBorderOffset[i]
                self.btnBorders[i].alpha = 1
            }
            
            }, completion: nil)
    }
    
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrOffset = scrollView.contentOffset.y
        
        if (scrollView == scrView)
        {
            if (scrOffset < 0)
            {
                coverPhoto.frame.origin.y = scrOffset
                coverPhoto.frame.size.height = 240 + scrOffset * -1
            }
            
            if (scrOffset < previousOffsetY)
            {
                forceCancel = true
            }
            else if (scrOffset > previousOffsetY)
            {
                forceCancel = false
            }
            
            previousOffsetY = scrOffset
            
            if (scrOffset >= 80 + (scrView.contentSize.height - scrView.frame.height) && goBackState == "state1" && autoGoBackReady)
            {
                goBackState = "state2"
                vc.viewSwitchLabel.showFromBelow(text: "Release to go to settings")
            }
            else if (scrOffset <= 80 + (scrView.contentSize.height - scrView.frame.height) && goBackState == "state2" && autoGoBackReady)
            {
                goBackState = "state1"
                vc.viewSwitchLabel.hide()
            }
        }
        else
        {
            
            if (scrOffset > previousOffsetY)
            {
                forceCancel = true
            }
            else if (scrOffset < previousOffsetY)
            {
                forceCancel = false
            }
            
            previousOffsetY = scrOffset
            
            if (scrOffset <= -80 && goBackState == "state1" && autoGoBackReady)
            {
                goBackState = "state2"
                vc.viewSwitchLabel.showFromAbove(text: "Release to go back")
            }
            else if (scrOffset >= -80 && goBackState == "state2" && autoGoBackReady)
            {
                goBackState = "state1"
                vc.viewSwitchLabel.hide()
            }
        }
        
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (goBackState == "state2" && !forceCancel && autoGoBackReady)
        {
            goBackState = "state3"
            
            if (scrollView == scrView)
            {
                UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
                    self.vc.viewSwitchLabel.frame.origin.y = -self.vc.viewSwitchLabel.frame.height
                },completion : {finsih in
                    self.goBackState = "state1"
                })
                self.showSettingView(UIButton())
            }
            else
            {
                UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
                    self.vc.viewSwitchLabel.frame.origin.y = self.vc.view.frame.height
                },completion : {finsih in
                    self.goBackState = "state1"
                })
                self.hideSettingView(UIButton())
            }
            
            vc.view.bringSubview(toFront: vc.viewSwitchLabel)
        }
    }
//
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        forceCancel = false
    }

    
}
