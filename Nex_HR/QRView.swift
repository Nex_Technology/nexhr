//
//  QRView.swift
//  Nex_HR
//
//  Created by Daud on 4/1/17.
//  Copyright © 2017 Daud. All rights reserved.
//

import Foundation
import UIKit
import AVFoundation
import JWTDecode

class QRView : UIView, AVCaptureMetadataOutputObjectsDelegate
{
    var captureSession : AVCaptureSession!
    var videoPreviewLayer : AVCaptureVideoPreviewLayer!
    var qrCodeFrame : UIView!
    let btnBack = UIButton()
    let lblDecode = UILabel()
    
    var alreadyDetected = false
    
    var popUpView = UIView()
    
    var encodeObj = ""
    var dateTime = ""
    
    var vc : ViewController!
    
    func initialize(vc : ViewController)
    {
        self.backgroundColor = UIColor.white
        self.vc = vc
        
        let captureDevice = AVCaptureDevice.defaultDevice(withMediaType: AVMediaTypeVideo)
        var error : NSError!
        
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
            
        } catch let error as NSError {
            print(error)
        }
        
        let captureMetaDataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetaDataOutput)
        
        captureMetaDataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
        captureMetaDataOutput.metadataObjectTypes = [AVMetadataObjectTypeQRCode]
        
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        videoPreviewLayer?.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height)
        self.layer.addSublayer(videoPreviewLayer)
        
        captureSession.startRunning()
        
        //Setting up grrenbox------------------------------------------
        
        qrCodeFrame = UIView()
        qrCodeFrame.layer.borderColor = UIColor.green.cgColor
        qrCodeFrame.layer.borderWidth = 5
        self.addSubview(qrCodeFrame)
        self.bringSubview(toFront: qrCodeFrame)
        
        btnBack.setTitle("Back", for: .normal)
        btnBack.setTitleColor(UIColor.white, for: .normal)
        btnBack.titleLabel?.textAlignment = .center
        btnBack.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnBack.frame = CGRect(x: 20, y: 20, width: 20, height: 20)
        btnBack.sizeToFit()
        btnBack.addTarget(self, action: #selector(self.goBack(_:)), for: .touchUpInside)
        self.addSubview(btnBack)
        
        lblDecode.text = ""
        lblDecode.font = UIFont(name: "SFUIText-Bold", size: 18)
        lblDecode.textColor = UIColor.white
        lblDecode.frame = self.frame
        lblDecode.numberOfLines = 0
        self.addSubview(lblDecode)
        
        self.alpha = 0
        UIView.animate(withDuration: 0.4, animations: {
            self.alpha = 1
        })

        
    }
    
    //Delegate Method-------------------------------------
    
    func captureOutput(_ captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [Any]!, from connection: AVCaptureConnection!) {
        
        
        if metadataObjects == nil || metadataObjects.count == 0 {
            qrCodeFrame.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            lblDecode.text = ""
            print("not found")
            return
        }
        
        // Get the metadata object.
        let metadataObj = metadataObjects[0] as! AVMetadataMachineReadableCodeObject
        
        if metadataObj.type == AVMetadataObjectTypeQRCode {
            // If the found metadata is equal to the QR code metadata then update the status label's text and set the bounds
            let barCodeObject = videoPreviewLayer?.transformedMetadataObject(for: metadataObj)
            qrCodeFrame?.frame = barCodeObject!.bounds
            
            if metadataObj.stringValue != nil {
                
                if (!alreadyDetected)
                {
                    popUpView.alpha = 1
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
                        self.popUpView.frame.origin.y = self.frame.height - 150
                    })
                    decodeJWT(decodeStr: metadataObj.stringValue)
                }
                
            }
        }
    }
    
    //Decode JWT---------------------------------
    
    func decodeJWT(decodeStr : String)
    {
        do {
            let jwt = try decode(jwt: decodeStr)
            
//            print("\n This is JWT  :  \(jwt)\n")
            
            let endIndex = jwt.string.index(jwt.string.endIndex, offsetBy: -6)
            var truncated = jwt.string.substring(to: endIndex)
            
            do {
                
                let last5 = (jwt.string as! String).substring(from:(jwt.string as! String).index((jwt.string as! String).endIndex, offsetBy: -5))
                
                if (last5 == "NexHR")
                {
                    let jwtBodyDecode = try decode(jwt: jwt.body["sub"] as! String)
                    let jwtStringDatas = jwtBodyDecode.string.characters.split{$0 == " "}.map(String.init)
                    
                    qrCodeFrame.layer.borderColor = UIColor.green.cgColor
                    print("\(jwtStringDatas[0]) and \(jwtStringDatas[1]) \(jwtStringDatas[2])")
                    alreadyDetected = true
                    
                    QRScanStore().storeData(code : "\(jwtStringDatas[0])", dateTime : "\(jwtStringDatas[1]) \(jwtStringDatas[2])")
                    
                    //Store the last checkIn date
                    let date = Date()
                    
                    let userDefault = UserDefaults.standard
                    userDefault.set(date as! Date, forKey: "previousDate")
                    print("stored Date is \(date)")
                    
                    QRScanStore().uploadScans(vc: self.vc, qr: self)
//                    QRScanStore().clearData()
                    
                    btnBack.alpha = 0
                    
                    
                }
                else
                {
                    qrCodeFrame.layer.borderColor = UIColor.red.cgColor
                    print("no Nex HR")
                }
                
            } catch let error as NSError {
                qrCodeFrame.layer.borderColor = UIColor.red.cgColor
                print("decode failed 2nd Step")
            }
            
        } catch let error as NSError {
            qrCodeFrame.layer.borderColor = UIColor.red.cgColor
            print("decode failed 1st Step")
        }

    }
    
    //Go Back-----------------------------------
    
    func goBack(_ btn : UIButton)
    {
        vc.checkIn.backgroundTapped(UITapGestureRecognizer())
//        UIView.animate(withDuration: 0.4, animations: {
//            self.alpha = 0
//        }, completion : {finsih in
//            self.removeFromSuperview()
//        })
    }
    
    //Retry Capturing-------------------------------
    
    func retry(_ btn : UIButton)
    {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            self.popUpView.frame.origin.y = self.frame.height
        },completion : {finished in
            self.popUpView.alpha = 0
            self.alreadyDetected = false
        })
        
    }
    
}
