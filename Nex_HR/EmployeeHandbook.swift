//
//  EmployeeHandbook.swift
//  Nex_HR
//
//  Created by Daud on 11/29/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import Alamofire
import UIKit

class EmployeeHandbook : UIView
{
    var vc : ViewController!
    
    let btnBack = UIButton()
    let webView = UIWebView()
    
//    var requestedURL = ""
    
    func initialize(vc : ViewController)
    {
        self.vc = vc
        
        btnBack.frame = CGRect(x: 0, y: 0, width: 60, height: 60)
        btnBack.center = CGPoint(x: self.frame.width / 2, y: self.frame.height - 50)
        btnBack.backgroundColor = UIColor.black
        btnBack.layer.cornerRadius = btnBack.frame.width / 2
        btnBack.addTarget(self, action: #selector(self.goBack(_:)), for: .touchUpInside)
        btnBack.setImage(UIImage(named : "btnQuitPDF.png"), for: .normal)
        self.addSubview(btnBack)
        
        webView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 100)
        webView.scalesPageToFit = true
        webView.backgroundColor = UIColor.black
        webView.alpha = 0
        webView.layer.cornerRadius = 3
        webView.layer.masksToBounds = true
        self.addSubview(webView)
        
        vc.loadingLabelUp(label: "Updating PDF")
    }
    
//    func getPDFLink()
//    {
//        
//        let userDefault = UserDefaults.standard
//        
//        if (userDefault.string(forKey: "previousLink") != requestedURL)
//        {
//            userDefault.set(requestedURL, forKey: "previousLink")
//            userDefault.set(false, forKey : "downloadedPDF")
//            
//            let auth = vc.userInform.auth_token
//            
//            Alamofire.request("http://nex-hr.herokuapp.com/api/employee_handbooks?auth_token=\(auth)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil)
//                .validate()
//                .responseJSON { response in
//                    switch(response.result)
//                    {
//                    case .success(let JSON) :
//                        print(JSON)
//                        self.requestedURL = (JSON as! NSDictionary)["file"] as! String
//                        print(self.requestedURL)
//                        self.getPDF()
//                        
//                    case .failure(let error) :
//                        print(" ERROR IS \(error)")
//                    }
//            }
//            
//        }
//        else
//        {
//            getPDF()
//        }
//        
//    }
    
    func getPDF(requestedURL : String)
    {
        
        //Getting the document directory and append the file name behind it--------------
        
        let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last!
        let targetURL = docURL.appendingPathComponent("my.pdf")
        
        
        let userDefault = UserDefaults.standard
        if (userDefault.bool(forKey: "downloadedPDF") == false)
        {
            //Download the PDF---------------------------------------------------------------
            //-------------------------------------------------------------------------------
            //-------------------------------------------------------------------------------
            
            let sourceURL = URL(string: requestedURL)
            let urlRequest = NSURLRequest(url: sourceURL!)
            
            do {
                let theData = try NSURLConnection.sendSynchronousRequest(urlRequest as URLRequest, returning: nil)
                try theData.write(to: targetURL)
                vc.loadingView.succeeded()
                userDefault.set(true, forKey : "downloadedPDF")
                
            } catch (let writeError) {
                
                print("error : \(writeError)")
                vc.loadingView.failed()
            }
        }
        
        

        
        
        //Checking the list of files in document directory-------------------------------
        //-------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------
        
//        do{
//            let contents = try (FileManager.default.contentsOfDirectory(at: docURL, includingPropertiesForKeys: nil, options: FileManager.DirectoryEnumerationOptions.skipsHiddenFiles))
//            print("There are")
//            print(contents)
//        }
//        catch (let error)
//        {
//            print("error contents \(error)")
//        }
        
        
        //Loading the PDF into web view--------------------------------------------------
        //-------------------------------------------------------------------------------
        //-------------------------------------------------------------------------------
        
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
        let documentsDirectory = paths[0]
        let finalURL = URL(fileURLWithPath: documentsDirectory).appendingPathComponent("my.pdf")
        let request = URLRequest(url: finalURL)
        
        webView.loadRequest(request)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.webView.alpha = 1
        })
        vc.loadingView.succeeded()
        
    }
    
    func goBack(_ btn : UIButton)
    {
        UIView.animate(withDuration: 0.2, animations: {
            self.btnBack.transform = CGAffineTransform(rotationAngle: 90)
        })
        
        UIView.animate(withDuration: 0.5, delay: 0.1, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.vc.view.frame.height
            self.vc.employeeMenu.frame.origin.y = 0
            
        },completion : { finish in
            
            UIView.animate(withDuration: 0.3, animations: {
                self.vc.homeView.alpha = 1
                self.vc.employeeMenu.alpha = 1
                self.vc.view.backgroundColor = UIColor.white
            },completion : { finish in
                
                UIView.animate(withDuration: 0.2, animations: {
                    
                    for cc in self.vc.employeeMenu.borders
                    {
                        cc.alpha = 1
                    }
                    
                })
                
//                self.vc.statusBarHidden = false
                self.removeFromSuperview()
            })
//
        })
    }
    
}
