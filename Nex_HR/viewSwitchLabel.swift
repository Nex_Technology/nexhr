//
//  viewSwitchLabel.swift
//  Nex_HR
//
//  Created by Daud on 12/2/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class ViewSwitchLabel: UILabel {

    var vc : ViewController!
    var showedDirection : Int!
    
    func initializer(vc : ViewController)
    {
        self.vc = vc
        
        self.textColor = UIColor.white
        self.alpha = 0.4
        self.font = UIFont(name: "SFUIText-Medium", size: 16)
        self.backgroundColor = UIColor(red: 152 / 255, green: 157 / 255, blue: 159 / 255, alpha: 1)
        self.textAlignment = .center
        self.clipsToBounds = true
    }
    
    func showFromAbove(text : String)
    {
        self.text = text
        self.sizeToFit()
        self.frame = CGRect(x: 0, y: 0, width: self.frame.width + 30, height: self.frame.height + 15)
        self.layer.cornerRadius = self.frame.height / 2
        self.center.x = vc.view.frame.width / 2
        showedDirection = 0
        vc.view.bringSubview(toFront: self)
        
        self.frame.origin.y = -self.frame.height
        UIView.animate(withDuration: 0.3, animations: {
            
            self.center.y = 60
            
        })
    }
    
    func showFromBelow(text : String)
    {
        self.text = text
        self.sizeToFit()
        self.frame = CGRect(x: 0, y: 0, width: self.frame.width + 30, height: self.frame.height + 15)
        self.layer.cornerRadius = self.frame.height / 2
        self.center.x = vc.view.frame.width / 2
        showedDirection = 1
        vc.view.bringSubview(toFront: self)
        
        self.frame.origin.y = vc.view.frame.height
        UIView.animate(withDuration: 0.2, animations: {
            
            self.center.y = self.vc.view.frame.height - 60
            
        })
    }
    
    func hide()
    {
        vc.view.bringSubview(toFront: self)
        if (showedDirection == 0)
        {
            UIView.animate(withDuration: 0.2, animations: {
                
                self.frame.origin.y = -self.frame.height
                
            })
        }
        else
        {
            UIView.animate(withDuration: 0.2, animations: {
                
                self.frame.origin.y = self.vc.view.frame.height
                
            })
        }
    }

    
}
