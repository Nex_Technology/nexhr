//
//  TextBoxCell.swift
//  Nex_HR
//
//  Created by Daud on 10/20/16.
//  Copyright © 2016 Daud. All rights reserved.
//


//A text box

import UIKit

class TextBoxCell: UITableViewCell, UITextFieldDelegate {

    let txt = UITextField()
    var vc : ViewController!
    var parentClass = ""
    var cellNum = 0
    var txtColor = UIColor()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?)
    {
        super.init(style : style, reuseIdentifier : reuseIdentifier)
        txt.font = UIFont(name: "SFUIText-Medium", size: 16)
        txt.delegate = self
        self.addSubview(txt)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func initialize(vc : ViewController, lbl2 : String, parentClass : String, cellNum : Int, txtColor : UIColor)
    {
        self.vc = vc
        self.parentClass = parentClass
        self.cellNum = cellNum
        self.txtColor = txtColor
        
        txt.text = lbl2
        txt.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 40)
        txt.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        txt.returnKeyType = .next
        txt.center.y = self.frame.height / 2
        txt.textColor = txtColor
        txt.addTarget(self, action: #selector(self.txtChanged), for: .editingChanged)
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if (parentClass == "leaveView")
        {
            vc.leaveFormView.scrollIndex = self.cellNum
            if(txt.text == vc.leaveFormView.lblInit[cellNum])
            {
                txt.text = ""
                vc.leaveFormView.curReasonColor = UIColor.black
                txt.textColor = vc.leaveFormView.curReasonColor
            }
            
        }
        else if (parentClass == "overtimeView")
        {
            vc.overtimeFormView.scrollIndex = self.cellNum
            if(txt.text == vc.overtimeFormView.lblInit[cellNum])
            {
                txt.text = ""
                vc.overtimeFormView.txtColor[cellNum] = UIColor.black
                txt.textColor = vc.overtimeFormView.txtColor[cellNum]
            }
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (parentClass == "leaveView")
        {
            if (txt.text == "")
            {
                txt.text = vc.leaveFormView.lblInit[cellNum]
                vc.leaveFormView.curReasonColor = UIColor(red: 203/255, green : 203/255, blue : 203/255, alpha : 1)
                txt.textColor = vc.leaveFormView.curReasonColor
            }
            
        }
        else if (parentClass == "overtimeView")
        {
            if (txt.text == "")
            {
                txt.text = vc.overtimeFormView.lblInit[cellNum]
                vc.overtimeFormView.txtColor[cellNum] = UIColor(red: 203/255, green : 203/255, blue : 203/255, alpha : 1)
                txt.textColor = vc.overtimeFormView.txtColor[cellNum]
            }
        }
    }
    
    func txtChanged()
    {
        if (parentClass == "leaveView")
        {
            vc.leaveFormView.curReason = txt.text!
        }
        else if (parentClass == "overtimeView")
        {
            vc.overtimeFormView.lblValues[cellNum] = txt.text!
        }
    }
    

}
