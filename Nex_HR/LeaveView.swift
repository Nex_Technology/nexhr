//
//  LeaveView.swift
//  Nex_HR
//
//  Created by Daud on 9/30/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class LeaveView: UIView, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    var sideAlignment : CGFloat = 20
    var JSON = NSDictionary()
    
    var vc : ViewController!
    
    var curMonth = "October"
    var curYear = "2016"
    var curYearNum = 0
    var curMonthNum = 0
    var curMonthIndex = 0
    var curYearIndex = 0
    var curDayNum = 0
    
    //Leaves Available-----------
    var casualTotal = 0
    var casualAvail = 0
    var casualUsed = 0
    var annualTotal = 0
    var annualAvail = 0
    var annualUsed = 0
    var medicalTotal = 0
    var medicalAvail = 0
    var medicalUsed = 0
    var marternityTotal = 0
    var marternityAvail = 0
    var marternityUsed = 0
    var compassionateTotal = 0
    var compassionateAvail = 0
    var compassionateUsed = 0
    var unpaidTotal = 0
    var unpaidAvail = 0
    var unpaidUsed = 0
    var otherTotal = 0
    var otherAvail = 0
    var otherUsed = 0
    var goBackState = "state1"
    var forceCancel = false
    var previousOffsetY : CGFloat = 0
    var autoGoBackReady = false
    
    var leaveTitles : [String] = ["Casual Leaves", "Annual Leaves", "Medicine Leaves","Pending Leaves", "Other Leaves"]
    var leaveTotals : [Int] = [0,0,0,0,-1]
    var leaveAvails : [Int] = [0,0,0,0,0]
    var leaveUses : [Int] = [0,0,0,0,0]
    var leaveCellHeight : [CGFloat] = [80,80,80,80,80]
    var leaveStartDates : [[String]] = [[String](),[String](),[String](),[String](),[String]()]
    var leaveEndDates : [[String]] = [[String](),[String](),[String](),[String](),[String]()]
    var leaveDurations : [[String]] = [[String](),[String](),[String](),[String](),[String]()]
    
    var leaveTypes = [String]()
    var froms = [String]()
    var tos = [String]()
    var status = [String]()
    
    var leaveColors = [UIColor]()
    var leaveDays = [String]()
    var leaveTags = [Int]()
    
    var leaveColor : [UIColor] = [UIColor(red: 0, green: 204/255, blue: 126/255, alpha: 1),UIColor(red: 255/255, green: 175/255, blue: 0/255, alpha: 1),UIColor(red: 0/255, green: 180/255, blue: 218/255, alpha: 1),UIColor(red: 187/255, green: 187/255, blue: 187/255, alpha: 1),UIColor(red: 255/255, green: 109/255, blue: 109/255, alpha: 1)]
    
    let scrView = UIScrollView()
    let headerCover = UIView()
    let tabView = UITableView()
    let btnDatePick = UIButton()
    var calenView : CalendarView!
    let summaryView = UIView()
//    let btnSummary = UIButton(type : .system)
    let btnRequestLeave = UIButton(type : .system)
    let indicatorContainer = UIView()
    let leaveListTab = UIScrollView()
    let lblLeave = UIButton()
    let segmentBtn = UISegmentedControl(items: ["Summary","List"])
    
    //LeaveForm Settings
    
    var leaveTypesList = [String]()
    var approvedBys = [String]()
    var reportTos = [String]()
    
    func initialize(_ vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = UIColor.clear
        
        var JSON = [NSDictionary]()
        
        curMonthNum = vc.userInform.curMonthNum
        curYearNum = vc.userInform.curYearNum
        curMonth = vc.userInform.curMonth
        curYear = vc.userInform.curYear
        curMonthIndex = vc.userInform.curMonthIndex
        curYearIndex = vc.userInform.curYearIndex
        curDayNum = vc.userInform.curDayNum
        
        leaveColor[0] = vc.userInform.leaveColors[0]
        leaveColor[1] = vc.userInform.leaveColors[1]
        leaveColor[2] = vc.userInform.leaveColors[2]
        
        //Scroll View-------
        
        scrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 80)
        scrView.contentSize.height = 1640
        scrView.delegate = self
        scrView.delaysContentTouches = false
        scrView.showsVerticalScrollIndicator = false
        self.addSubview(scrView)
        
        //Header labels, back buttons and header view--------
        
        lblLeave.setTitle("Leave", for: .normal)
        lblLeave.setTitleColor(UIColor.black, for: .normal)
        lblLeave.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblLeave.sizeToFit()
        lblLeave.frame.origin.x = sideAlignment
        lblLeave.center.y = 50
        scrView.addSubview(lblLeave)
        
        let btnHome = UIButton(frame: CGRect(x: self.frame.width - sideAlignment - 35, y: sideAlignment , width: 35, height: 35))
        btnHome.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnHome.center.y = 50
        btnHome.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        scrView.addSubview(btnHome)
        
        let lblCurDate = UILabel()
        lblCurDate.text = "Today: \(curDayNum).\(curMonth).\(curYear)"
        lblCurDate.textColor = UIColor.black
        lblCurDate.font = UIFont(name: "SFUIText-Regular", size: 16)
        lblCurDate.sizeToFit()
        lblCurDate.frame.origin.x = sideAlignment
        lblCurDate.center.y = 80
        lblCurDate.alpha = 0.6
        scrView.addSubview(lblCurDate)
        
        headerCover.frame = CGRect(x: 0, y: -20, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 0
        self.addSubview(headerCover)
        
        let lblLeave2 = UILabel()
        lblLeave2.text = "Leave"
        lblLeave2.textColor = UIColor.black
        lblLeave2.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblLeave2.sizeToFit()
        lblLeave2.frame.origin.x = sideAlignment
        lblLeave2.center.y = headerCover.frame.height / 2
        headerCover.addSubview(lblLeave2)
        
        let btnHome2 = UIButton(frame: CGRect(x:  self.frame.width - 35 - sideAlignment, y: sideAlignment , width: 35, height: 35))
        btnHome2.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome2.imageView?.contentMode = .scaleAspectFit
        btnHome2.center.y = headerCover.frame.height / 2
        btnHome2.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        headerCover.addSubview(btnHome2)
        
        //label and button to choose date------
        
        let lblChooseDate = UILabel(frame : CGRect(x: sideAlignment, y: 130, width: 100, height: 100))
        lblChooseDate.text = "Past Leave Summary of"
        lblChooseDate.textColor = UIColor.black
        lblChooseDate.font = UIFont(name: "SFUIText-Medium", size: 18)
        lblChooseDate.sizeToFit()
        scrView.addSubview(lblChooseDate)
        
        changeButtonDate()
        btnDatePick.setTitleColor(UIColor.white, for: .normal)
        btnDatePick.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 16)
        btnDatePick.backgroundColor = UIColor(red: 0/255, green: 175/255, blue: 255/255, alpha: 1)
        btnDatePick.layer.cornerRadius = 6
        btnDatePick.titleLabel?.textAlignment = .center
        btnDatePick.frame.origin.x = sideAlignment
        btnDatePick.frame.origin.y = lblChooseDate.frame.origin.y + lblChooseDate.frame.height + 8
        btnDatePick.addTarget(self, action: #selector(self.chooseDate(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
        scrView.addSubview(btnDatePick)
        
        summaryView.backgroundColor = UIColor.clear
        summaryView.frame = CGRect(x: 0, y: 0,width : 180, height: 35)
        summaryView.center.x = self.frame.width / 2
        summaryView.frame.origin.y = btnDatePick.frame.origin.y + btnDatePick.frame.height + 50
        scrView.addSubview(summaryView)
        
        segmentBtn.frame = CGRect(x: 0, y: 0, width: 180, height: 35)
        segmentBtn.selectedSegmentIndex = 0
        segmentBtn.tintColor = UIColor(red : 0.2, green : 0.25, blue : 0.3, alpha : 1)
        segmentBtn.layer.cornerRadius = 6
        segmentBtn.layer.borderColor = UIColor(red : 0.2, green : 0.25, blue : 0.3, alpha : 1).cgColor
        segmentBtn.layer.borderWidth = 2
        segmentBtn.layer.masksToBounds = true
        segmentBtn.addTarget(self, action: #selector(self.changeSummary(_:)), for: .valueChanged)
        summaryView.addSubview(segmentBtn)
        
        
        //Calender view--------
        
        setupCalendar()
        
        //Leave type indicator------
        
        
        indicatorContainer.frame = CGRect(x: 0, y: calenView.frame.origin.y + calenView.frame.height + 30, width: self.frame.width, height: 300)
        indicatorContainer.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        scrView.addSubview(indicatorContainer)
        
        let leaveTypeLabel = UILabel(frame : CGRect(x: sideAlignment, y: 0, width: self.frame.width - sideAlignment * 2, height: 20))
        leaveTypeLabel.text = "This color means this type"
        leaveTypeLabel.alpha = 0.4
        leaveTypeLabel.textColor = UIColor.black
        leaveTypeLabel.font = UIFont(name: "SFUIText-Medium", size: 15)
        leaveTypeLabel.sizeToFit()
        indicatorContainer.addSubview(leaveTypeLabel)
        
        let greenView = UIView(frame : CGRect(x: 20, y: 50, width: 12, height: 12))
        greenView.layer.cornerRadius = greenView.frame.width / 2
//        greenView.backgroundColor = UIColor(red: 0, green: 204/255, blue: 126/255, alpha: 1)
        greenView.backgroundColor = leaveColor[0]
        indicatorContainer.addSubview(greenView)
        
        let lblCasual = UILabel(frame : CGRect(x: 42, y: greenView.frame.origin.y, width: 100, height: 100))
        lblCasual.text = "Casual Leave"
        lblCasual.textColor = UIColor.black
        lblCasual.font = UIFont(name: "SFUIText-Medium", size: 14)
        lblCasual.sizeToFit()
        lblCasual.center.y = greenView.center.y
        indicatorContainer.addSubview(lblCasual)
        
        let yellowView = UIView(frame : CGRect(x: 20, y: 90, width: 12, height: 12))
        yellowView.layer.cornerRadius = yellowView.frame.width / 2
        //        yellowView.backgroundColor = UIColor(red: 255/255, green: 175/255, blue: 0/255, alpha: 1)
        yellowView.backgroundColor = leaveColor[1]
        indicatorContainer.addSubview(yellowView)
        
        let lblAnnualLeave = UILabel(frame : CGRect(x: 42, y: yellowView.frame.origin.y, width: 100, height: 100))
        lblAnnualLeave.text = "Annual Leave"
        lblAnnualLeave.textColor = UIColor.black
        lblAnnualLeave.font = UIFont(name: "SFUIText-Medium", size: 14)
        lblAnnualLeave.sizeToFit()
        lblAnnualLeave.center.y = yellowView.center.y
        indicatorContainer.addSubview(lblAnnualLeave)
        
        let blueView = UIView(frame : CGRect(x: 20, y: 130, width: 12, height: 12))
        blueView.layer.cornerRadius = blueView.frame.width / 2
//        blueView.backgroundColor = UIColor(red: 0/255, green: 180/255, blue: 218/255, alpha: 1)
        blueView.backgroundColor = leaveColor[2]
        indicatorContainer.addSubview(blueView)
        
        let lblMedicine = UILabel(frame : CGRect(x: 42, y: blueView.frame.origin.y, width: 100, height: 100))
        lblMedicine.text = "Medicine Leave"
        lblMedicine.textColor = UIColor.black
        lblMedicine.font = UIFont(name: "SFUIText-Medium", size: 14)
        lblMedicine.sizeToFit()
        lblMedicine.center.y = blueView.center.y
        indicatorContainer.addSubview(lblMedicine)
        
        let grayView = UIView(frame : CGRect(x: 20, y: 170, width: 12, height: 12))
        grayView.layer.cornerRadius = grayView.frame.width / 2
        grayView.backgroundColor = UIColor(red: 187/255, green: 187/255, blue: 187/255, alpha: 1)
        indicatorContainer.addSubview(grayView)
        
        let lblPending = UILabel(frame : CGRect(x: 42, y: grayView.frame.origin.y, width: 100, height: 100))
        lblPending.text = "Pending Leave"
        lblPending.textColor = UIColor.black
        lblPending.font = UIFont(name: "SFUIText-Medium", size: 14)
        lblPending.sizeToFit()
        lblPending.center.y = grayView.center.y
        indicatorContainer.addSubview(lblPending)
        
        let redView = UIView(frame : CGRect(x: 20, y: 210, width: 12, height: 12))
        redView.layer.cornerRadius = redView.frame.width / 2
        redView.backgroundColor = UIColor(red: 255/255, green: 109/255, blue: 109/255, alpha: 1)
        indicatorContainer.addSubview(redView)
        
        let lblOther = UILabel(frame : CGRect(x: 42, y: redView.frame.origin.y, width: 100, height: 100))
        lblOther.text = "Other Leave"
        lblOther.textColor = UIColor.black
        lblOther.font = UIFont(name: "SFUIText-Medium", size: 14)
        lblOther.sizeToFit()
        lblOther.center.y = redView.center.y
        indicatorContainer.addSubview(lblOther)
        
        //table view to show leave lists------
        
        tabView.dataSource = self
        tabView.delegate = self
        tabView.register(LeaveListView.self, forCellReuseIdentifier: "leaveListView")
        tabView.frame = CGRect(x: sideAlignment, y: indicatorContainer.frame.origin.y + indicatorContainer.frame.height + 20 , width: self.frame.width - sideAlignment, height: 120 * 5)
        tabView.separatorColor = UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1)
        scrView.contentSize.height = tabView.frame.origin.y + tabView.frame.size.height + 40
        scrView.addSubview(tabView)
        
        //btn request leave-----
        
        btnRequestLeave.setTitle("Make A Form", for: .normal)
        btnRequestLeave.setTitleColor(UIColor.black, for: .normal)
        btnRequestLeave.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 20)
        btnRequestLeave.frame = CGRect(x: 0, y: self.frame.height - 80, width: self.frame.width, height: 80)
        btnRequestLeave.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        btnRequestLeave.addTarget(self, action: #selector(self.requestLeaveForm(_:)), for: .touchUpInside)
        btnRequestLeave.layer.addBorder(.top, color: UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha : 1), thickness: 1)
        self.addSubview(btnRequestLeave)
        
        let btnView = UIView(frame : CGRect(x: sideAlignment, y: 0, width: 15, height: 15))
        btnView.backgroundColor = UIColor(red: 255/255, green: 174/255, blue: 14/255, alpha: 1)
        btnView.layer.cornerRadius = btnView.frame.height / 2
        btnView.center.y = 40
        btnRequestLeave.addSubview(btnView)
        
        scrView.addSubview(leaveListTab)
        
    }
    
    //Change Summary and list-----------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func changeSummary(_ btn : UISegmentedControl)
    {
        if (btn.selectedSegmentIndex == 1)
        {
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.calenView.frame.origin.x = -self.frame.width
                self.tabView.frame.origin.x = -self.frame.width
                self.indicatorContainer.frame.origin.x = -self.frame.width
                self.leaveListTab.frame.origin.x = self.sideAlignment
                
            }, completion: nil)
            
        }
        else{
            
            UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.calenView.frame.origin.x = self.sideAlignment
                self.tabView.frame.origin.x = self.sideAlignment
                self.indicatorContainer.frame.origin.x = 0
                self.leaveListTab.frame.origin.x = self.sideAlignment + self.frame.width
                
            }, completion: nil)
        }
    }
    
    //Setup Calendar View-----------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func setupCalendar()
    {
        calenView = CalendarView()
        
        if (segmentBtn.selectedSegmentIndex == 0)
        {
            calenView.frame = CGRect(x: sideAlignment, y: summaryView.frame.origin.y + summaryView.frame.height + 50, width: self.frame.width - sideAlignment * 2, height: 0)
        }
        else
        {
            calenView.frame = CGRect(x: -self.frame.width, y: summaryView.frame.origin.y + summaryView.frame.height + 50, width: self.frame.width - sideAlignment * 2, height: 0)
        }
        
        calenView.frame.size.height = (calenView.frame.width)
        calenView.initialize(vc: self.vc, leaveColors : leaveColors, leaveTags : leaveTags, leaveDays : leaveDays)
        scrView.addSubview(calenView)
        
    }
    
    //Setup LeaveList View----------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func setupLeaveListView()
    {
        leaveListTab.contentSize.width = 470
        
        if (segmentBtn.selectedSegmentIndex == 0)
        {
            leaveListTab.frame = CGRect(x: self.frame.width + sideAlignment, y: summaryView.frame.origin.y + summaryView.frame.height + 50, width: self.frame.width - sideAlignment * 2, height: 0)
        }
        else
        {
            leaveListTab.frame = CGRect(x: sideAlignment, y: summaryView.frame.origin.y + summaryView.frame.height + 50, width: self.frame.width - sideAlignment * 2, height: 0)
        }
        leaveListTab.frame.size.height = 0
        
        let lblTitle = UILabel(frame : CGRect(x: 0, y: 0, width: 140, height: 40))
        lblTitle.text = "Leave Type"
        lblTitle.textColor = UIColor.black
        lblTitle.font = UIFont(name: "SFUIText-Bold", size: 16)
        lblTitle.sizeToFit()
        leaveListTab.addSubview(lblTitle)
        
        let lblTitle2 = UILabel(frame : CGRect(x: 140, y: 0, width: 120, height: 40))
        lblTitle2.text = "Start Date"
        lblTitle2.textColor = UIColor.black
        lblTitle2.font = UIFont(name: "SFUIText-Bold", size: 16)
        lblTitle2.sizeToFit()
        leaveListTab.addSubview(lblTitle2)
        
        let lblTitle3 = UILabel(frame : CGRect(x: 260, y: 0, width: 120, height: 40))
        lblTitle3.text = "End Date"
        lblTitle3.textColor = UIColor.black
        lblTitle3.font = UIFont(name: "SFUIText-Bold", size: 16)
        lblTitle3.sizeToFit()
        leaveListTab.addSubview(lblTitle3)
        
        let lblTitle4 = UILabel(frame : CGRect(x: 380, y: 0, width: 100, height: 40))
        lblTitle4.text = "Status"
        lblTitle4.textColor = UIColor.black
        lblTitle4.font = UIFont(name: "SFUIText-Bold", size: 16)
        lblTitle4.sizeToFit()
        leaveListTab.addSubview(lblTitle4)
        
        for var i in (0 ..< leaveTypes.count)
        {
            
            let lbl = UILabel(frame : CGRect(x: 0, y: (i * 40) + 60, width: 140, height: 40))
            lbl.text = leaveTypes[i]
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Medium", size: 14)
            lbl.sizeToFit()
            lbl.alpha = 0.7
            leaveListTab.addSubview(lbl)
            
            let lbl2 = UILabel(frame : CGRect(x: 140, y: (i * 40) + 60, width: 120, height: 40))
            lbl2.text = froms[i]
            lbl2.textColor = UIColor.black
            lbl2.font = UIFont(name: "SFUIText-Medium", size: 14)
            lbl2.sizeToFit()
            lbl2.alpha = 0.7
            leaveListTab.addSubview(lbl2)
            
            let lbl3 = UILabel(frame : CGRect(x: 260, y: (i * 40) + 60, width: 120, height: 40))
            lbl3.text = tos[i]
            lbl3.textColor = UIColor.black
            lbl3.font = UIFont(name: "SFUIText-Medium", size: 14)
            lbl3.sizeToFit()
            lbl3.alpha = 0.7
            leaveListTab.addSubview(lbl3)
            
            let lbl4 = UILabel(frame : CGRect(x: 380, y: (i * 40) + 60, width: 100, height: 40))
            lbl4.text = status[i]
            lbl4.textColor = UIColor.black
            lbl4.font = UIFont(name: "SFUIText-Medium", size: 14)
            lbl4.sizeToFit()
            lbl4.alpha = 0.7
            leaveListTab.addSubview(lbl4)
            
            leaveListTab.frame.size.height = lbl.frame.origin.y + lbl.frame.height + 30
            
        }
        scrView.contentSize.height = leaveListTab.frame.origin.y + leaveListTab.frame.height
        
    }
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func api_Post()
    {
        self.vc.loadingLabelUp(label: "Requesting..")
        
        resetProperties()
        
        let auth = vc.userInform.auth_token
        let parameters = [
            "month" : "\(curMonthNum)",
            "year" : "\(curYearNum)"
        ]
        
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)/leaves?auth_token=\(auth)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.vc.loadingView.succeeded()
                    self.autoGoBackReady = true
                    self.resetProperties()
                    if let JSONtemp = JSON as? NSDictionary
                    {
                        self.JSON = JSON as! NSDictionary
                        self.setResults(JSON : JSON as! NSDictionary)
                    }
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                }
        }
        
        
    }
    
    func setResults(JSON : NSDictionary)
    {
        leaveTags.removeAll()
        leaveDays.removeAll()
        
        //set LeaveCounts -------------------
        let leavesUsed = JSON["leave_count"] as! [NSDictionary]
        let leavesTotal = JSON["available_leave_count"] as! [NSDictionary]
        
        var leaveUsed = leavesUsed[0]
        casualUsed = leaveUsed["count"] as! Int
        leaveUsed = leavesUsed[1]
        annualUsed = leaveUsed["count"] as! Int
        leaveUsed = leavesUsed[2]
        medicalUsed = leaveUsed["count"] as! Int
        leaveUsed = leavesUsed[3]
        marternityUsed = leaveUsed["count"] as! Int
        leaveUsed = leavesUsed[4]
        compassionateUsed = leaveUsed["count"] as! Int
        leaveUsed = leavesUsed[5]
        unpaidUsed = leaveUsed["count"] as! Int
        otherUsed = marternityUsed + compassionateUsed + unpaidUsed
        leaveUses = [casualUsed,annualUsed,medicalUsed,0,otherUsed]
        
        var leaveTotal = leavesTotal[0]
        casualTotal = leaveTotal["count"] as! Int
        leaveTotal = leavesTotal[1]
        annualTotal = leaveTotal["count"] as! Int
        leaveTotal = leavesTotal[2]
        medicalTotal = leaveTotal["count"] as! Int
        leaveTotal = leavesTotal[3]
        marternityTotal = leaveTotal["count"] as! Int
        leaveTotal = leavesTotal[4]
        compassionateTotal = leaveTotal["count"] as! Int
        leaveTotal = leavesTotal[5]
        unpaidTotal = leaveTotal["count"] as! Int
        leaveTotals = [casualTotal,annualTotal,medicalTotal,0,0]
        
        casualAvail = casualTotal - casualUsed
        annualAvail = annualTotal - annualUsed
        medicalAvail = medicalTotal - medicalUsed
        leaveAvails = [casualAvail,annualAvail,medicalAvail,0,0]
        
        let leaves = JSON["leaves"] as! [NSDictionary]
        
        for var i in(0 ..< leaves.count)
        {
            let leave = leaves[i]
            let leaveType = leave["leave_type"] as! NSDictionary
            let leaveTypeStr = leaveType["name"] as! String
            let leaveStartDate = leave["start_date"] as! String
            let leaveEndDate = leave["end_date"] as! String
            let leaveDuration = "\(leaveStartDate) to \(leaveEndDate)"
            
            let leaveEndNum = Int(leaveEndDate.substring(from : leaveEndDate.index(leaveEndDate.startIndex, offsetBy: 8)))
            let leaveStartNum = Int(leaveStartDate.substring(from : leaveStartDate.index(leaveStartDate.startIndex, offsetBy: 8)))
            
            let leaveStatus = leave["leave_status"] as! NSDictionary
            let leaveStatusStr = leaveStatus["name"] as! String
            
            if(leaveStatusStr == "Pending")
            {
                leaveCellHeight[3] = leaveCellHeight[3] + 40
                tabView.frame.size.height = tabView.frame.height + 40
                leaveStartDates[3].append(leaveStartDate)
                leaveEndDates[3].append(leaveEndDate)
                leaveDurations[3].append(leaveDuration)
                leaveTotals[3] = leaveTotals[3] + 1
                
            }
            
            if(leaveTypeStr == "Casual Leave")
            {
                leaveCellHeight[0] = leaveCellHeight[0] + 40
                tabView.frame.size.height = tabView.frame.height + 40
                leaveStartDates[0].append(leaveStartDate)
                leaveEndDates[0].append(leaveEndDate)
                leaveDurations[0].append(leaveDuration)
                
                for var j in (leaveStartNum! ..< leaveEndNum! + 1)
                {
                    let leaveDayNum = "\(j)"
                    leaveDays.append(leaveDayNum)
                    leaveTags.append(i)
                    leaveColors.append(leaveColor[0])
//                    print("casual")
                }
                
            }
            else if(leaveTypeStr == "Annual Leave")
            {
                leaveCellHeight[1] = leaveCellHeight[1] + 40
                tabView.frame.size.height = tabView.frame.height + 40
                leaveStartDates[1].append(leaveStartDate)
                leaveEndDates[1].append(leaveEndDate)
                leaveDurations[1].append(leaveDuration)
                
                for var j in (leaveStartNum! ..< leaveEndNum! + 1)
                {
                    let leaveDayNum = "\(j)"
                    leaveDays.append(leaveDayNum)
                    leaveTags.append(i)
                    leaveColors.append(leaveColor[1])
                 }
                
            }
            else if(leaveTypeStr == "Medical Leave")
            {
                leaveCellHeight[2] = leaveCellHeight[2] + 40
                tabView.frame.size.height = tabView.frame.height + 40
                leaveStartDates[2].append(leaveStartDate)
                leaveEndDates[2].append(leaveEndDate)
                leaveDurations[2].append(leaveDuration)
                
                for var j in (leaveStartNum! ..< leaveEndNum! + 1)
                {
                    let leaveDayNum = "\(j)"
                    leaveDays.append(leaveDayNum)
                    leaveTags.append(i)
                    leaveColors.append(leaveColor[2])
                 }
                
            }
            else if(leaveTypeStr == "Pending Leave")
            {
                leaveCellHeight[3] = leaveCellHeight[3] + 40
                tabView.frame.size.height = tabView.frame.height + 40
                leaveStartDates[3].append(leaveStartDate)
                leaveEndDates[3].append(leaveEndDate)
                leaveDurations[3].append(leaveDuration)
                
                for var j in (leaveStartNum! ..< leaveEndNum! + 1)
                {
                    let leaveDayNum = "\(j)"
                    leaveDays.append(leaveDayNum)
                    leaveTags.append(i)
                    leaveColors.append(leaveColor[3])
                 }
                
            }
            else if(leaveTypeStr == "Maternity leave" || leaveTypeStr == "Compassionate leave" || leaveTypeStr == "Unpaid leave")
            {
                leaveCellHeight[4] = leaveCellHeight[4] + 40
                tabView.frame.size.height = tabView.frame.height + 40
                leaveStartDates[4].append(leaveStartDate)
                leaveEndDates[4].append(leaveEndDate)
                leaveDurations[4].append(leaveDuration)
                
                for var j in (leaveStartNum! ..< leaveEndNum! + 1)
                {
                    let leaveDayNum = "\(j)"
                    leaveDays.append(leaveDayNum)
                    leaveTags.append(i)
                    leaveColors.append(leaveColor[4])
                }
                
            }
            
            leaveTypes.append(leaveTypeStr)
            froms.append(leaveStartDate)
            tos.append(leaveEndDate)
            status.append(leaveStatusStr)
            
        }
        
        //setup calendar---------------------
        calenView.removeFromSuperview()
        setupCalendar()
        setupLeaveListView()
        
        tabView.reloadData()
        scrView.contentSize.height = tabView.frame.origin.y + tabView.frame.size.height + 40
        
    }
    
    //Resets UI, arrays and variables---------------------------------
    
    func resetProperties()
    {
        leaveTotals = [0,0,0,0,0]
        leaveAvails = [0,0,0,0,0]
        leaveUses = [0,0,0,0,0]
        leaveCellHeight = [80,80,80,80,80]
        leaveStartDates = [[String](),[String](),[String](),[String](),[String]()]
        leaveEndDates = [[String](),[String](),[String](),[String](),[String]()]
        leaveDurations = [[String](),[String](),[String](),[String](),[String]()]
        
        tabView.reloadData()
        tabView.frame.size.height = 80 * 5
        scrView.contentSize.height = tabView.frame.origin.y + tabView.frame.size.height + 40
        
        leaveTypes.removeAll()
        froms.removeAll()
        tos.removeAll()
        status.removeAll()
        
        for cc in leaveListTab.subviews
        {
            cc.removeFromSuperview()
        }
    }
    
    //Change the date on the button and request the api
    func changeButtonDate()
    {
        btnDatePick.setTitle("\(curMonth) \(curYear)", for: .normal)
        btnDatePick.sizeToFit()
        btnDatePick.frame.size.width = btnDatePick.frame.width + 20
        btnDatePick.frame.size.height = btnDatePick.frame.height + 5
        api_Post()
    }
    
    
    
    //Go Back to Home Screen--------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func goBackHome(_ btn : UIButton)
    {
        vc.homeView.viewBtns[1].alpha = 1
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.vc.homeView.btnBorders[1].alpha = 1
            
            }, completion: {finish in
                self.removeFromSuperview()
        })
        self.vc.homeView.comeBack(delay : 0.15)
    }
    
    
    //Choose Month and Year---------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func chooseDate(_ btn : UIButton)
    {
        vc.monthYearPicker = MonthYearPicker()
        vc.monthYearPicker.frame = vc.view.frame
        vc.monthYearPicker.initialize(vc, parentView : "LeaveView")
        vc.view.addSubview(vc.monthYearPicker)
    }
    
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrOffset = scrView.contentOffset.y
        
        if (scrOffset > previousOffsetY)
        {
            forceCancel = true
        }
        else if (scrOffset < previousOffsetY)
        {
            forceCancel = false
        }
        
        previousOffsetY = scrOffset
        
        if (scrOffset >= 110)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 1
                self.headerCover.frame.origin.y = 0
                
            }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 0
                self.headerCover.frame.origin.y = -20
                
            }, completion: nil)
        }
        
        
//        self.transform = CGAffineTransform(scaleX: 1 + ((scrOffset) * 0.0015), y: 1 + ((scrOffset) * 0.0015))
//        self.center.y = self.vc.view.frame.height / 2 - (scrOffset) * 0.5
        
        if (scrOffset <= -0 && (goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            self.transform = CGAffineTransform(scaleX: 1 + ((scrOffset + 0) * 0.0015), y: 1 + ((scrOffset + 0) * 0.0015))
            self.center.y = self.vc.view.frame.height / 2 - (scrOffset + 0) * 0.5
        }
        else if((goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.center.y = self.vc.view.frame.height / 2
                
            }, completion: nil)
        }
        
        if (scrOffset <= -80 && goBackState == "state1" && autoGoBackReady)
        {
            goBackState = "state2"
            vc.viewSwitchLabel.showFromAbove(text: "Release to to back")
        }
        else if (scrOffset >= -80 && goBackState == "state2" && autoGoBackReady)
        {
            goBackState = "state1"
            vc.viewSwitchLabel.hide()
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (goBackState == "state2" && !forceCancel && autoGoBackReady)
        {
            goBackState = "state3"
            vc.viewSwitchLabel.hide()
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.alpha = 0
                self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                self.frame.origin.y = self.vc.view.frame.height
                
            }, completion: {finish in
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.vc.homeView.viewBtns[1].alpha = 1
                })
                self.vc.homeView.comeBack(delay : 0)
                self.vc.homeView.btnBorders[1].alpha = 1
                self.removeFromSuperview()
                
            })
        }
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        forceCancel = false
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.center.y = self.vc.view.frame.height / 2
            
        }, completion: nil)

    }

    
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "leaveListView", for: indexPath as IndexPath) as! LeaveListView
        cell.initialize(leaveTitles[indexPath.row], color: leaveColor[indexPath.row], total: leaveTotals[indexPath.row], used: leaveUses[indexPath.row], endDate : leaveEndDates[indexPath.row], startDate : leaveStartDates[indexPath.row])
        cell.selectionStyle = .none
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return leaveCellHeight[indexPath.row]
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: false)
    }
    
    
    //Request Leave Form------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func requestLeaveForm(_ btn : UIButton)
    {
        vc.leaveFormView = LeaveFormView()
        vc.leaveFormView.frame = vc.view.frame
        vc.leaveFormView.initialize(self.vc)
        vc.view.addSubview(vc.leaveFormView)
        
        let blackCover = UIView(frame : self.frame)
        blackCover.backgroundColor = UIColor.black
        blackCover.alpha = 0
        self.addSubview(blackCover)
        
        vc.leaveFormView.frame.origin.y = self.frame.height
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = -50
            blackCover.alpha = 0.5
            self.vc.leaveFormView.frame.origin.y = 0
            
            }, completion: {finish in
                blackCover.removeFromSuperview()
        })
    }
    
    
    func comeBack()
    {
        let blackCover = UIView(frame : self.frame)
        blackCover.backgroundColor = UIColor.black
        blackCover.alpha = 0.5
        self.addSubview(blackCover)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = 0
            blackCover.alpha = 0
            
            }, completion: {finish in
                blackCover.removeFromSuperview()
        })
    }


}
