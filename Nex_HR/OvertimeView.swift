//
//  Overtime.swift
//  Nex_HR
//
//  Created by Daud on 10/11/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire

class OvertimeView: UIView, UIScrollViewDelegate {
    
    var sideAlignment : CGFloat = 20
    let titles : [String] = ["Total Working Hour","Total Late Hour"]
    let cellWidths : [CGFloat] = [110,110,110,120,120]
    let cellStart : [CGFloat] = [0,110,220,330,450]
    let cellTitles : [String] = ["OTDate","Start Time","End Time","Hour Apply","Total Hours"]
    let cellDays : [String] = ["S","M","T","W","T","F","S"]
    
    let tableView = UIScrollView()
    
    var vc : ViewController!
    
    var JSON : AnyObject!
    var curMonth = "October"
    var curYear = "2016"
    var curYearNum = 0
    var curMonthNum = 0
    var curMonthIndex = 0
    var curYearIndex = 0
    var curWeekNum = 0
    var curDayNum = 0
    var goBackState = "state1"
    var forceCancel = false
    var previousOffsetY : CGFloat = 0
    var autoGoBackReady = false
    
    let scrView = UIScrollView()
    let headerCover = UIView()
    //let lblDate : [UILabel] = [UILabel(),UILabel(),UILabel(),UILabel(),UILabel(),UILabel(),UILabel()]
    let btnDatePick = UIButton()
    var lblResults = [UILabel]()
    var btnWeeks = [UIButton]()
    var btnOvForm = UIButton()
    let lblOvertime = UIButton()
    
    func initialize(_ vc : ViewController)
    {
        self.vc = vc
        self.backgroundColor = UIColor.clear
        
        scrView.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: self.frame.height - 80)
        scrView.contentSize.height = 650
        scrView.delegate = self
        scrView.delaysContentTouches = false
        scrView.showsVerticalScrollIndicator = false
        self.addSubview(scrView)
        
        lblOvertime.setTitle("Overtime", for: .normal)
        lblOvertime.setTitleColor(UIColor.black, for: .normal)
        lblOvertime.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblOvertime.sizeToFit()
        lblOvertime.frame.origin.x = sideAlignment
        lblOvertime.center.y = 50
        scrView.addSubview(lblOvertime)
        
        let btnHome = UIButton(frame: CGRect(x: self.frame.width - sideAlignment - 35, y: sideAlignment , width: 35, height: 35))
        btnHome.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome.imageView?.contentMode = .scaleAspectFit
        btnHome.center.y = 50
        btnHome.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        scrView.addSubview(btnHome)
        
        headerCover.frame = CGRect(x: 0, y: -20, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 0
        self.addSubview(headerCover)
        
        let lblOvertime2 = UILabel()
        lblOvertime2.text = "Overtime"
        lblOvertime2.textColor = UIColor.black
        lblOvertime2.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblOvertime2.sizeToFit()
        lblOvertime2.frame.origin.x = sideAlignment
        lblOvertime2.center.y = headerCover.frame.height / 2
        headerCover.addSubview(lblOvertime2)
        
        let btnHome2 = UIButton(frame: CGRect(x: self.frame.width - 35 - sideAlignment, y: sideAlignment , width: 35, height: 35))
        btnHome2.setImage(UIImage(named: "btnHome2.png"), for: .normal)
        btnHome2.imageView?.contentMode = .scaleAspectFit
        btnHome2.center.y = headerCover.frame.height / 2
        btnHome2.addTarget(self, action: #selector(self.goBackHome(_:)), for: .touchUpInside)
        headerCover.addSubview(btnHome2)
        
        let lblChooseDate = UILabel(frame : CGRect(x: sideAlignment, y: 130, width: 100, height: 100))
        lblChooseDate.text = "My Overtime for"
        lblChooseDate.textColor = UIColor.black
        lblChooseDate.font = UIFont(name: "SFUIText-Medium", size: 18)
        lblChooseDate.sizeToFit()
        scrView.addSubview(lblChooseDate)
        
        curMonthNum = vc.userInform.curMonthNum
        curYearNum = vc.userInform.curYearNum
        curMonth = vc.userInform.curMonth
        curYear = vc.userInform.curYear
        curMonthIndex = vc.userInform.curMonthIndex
        curYearIndex = vc.userInform.curYearIndex
        curDayNum = vc.userInform.curDayNum
        
        let lblCurDate = UILabel()
        lblCurDate.text = "Today: \(curDayNum).\(curMonth).\(curYear)"
        lblCurDate.textColor = UIColor.black
        lblCurDate.font = UIFont(name: "SFUIText-Regular", size: 16)
        lblCurDate.sizeToFit()
        lblCurDate.frame.origin.x = sideAlignment
        lblCurDate.center.y = 80
        lblCurDate.alpha = 0.6
        scrView.addSubview(lblCurDate)
        
        changeButtonDate()
        btnDatePick.setTitleColor(UIColor.white, for: .normal)
        btnDatePick.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 16)
        btnDatePick.backgroundColor = UIColor(red: 0/255, green: 175/255, blue: 255/255, alpha: 1)
        btnDatePick.layer.cornerRadius = 6
        btnDatePick.titleLabel?.textAlignment = .center
        btnDatePick.frame.origin.x = sideAlignment
        btnDatePick.frame.origin.y = lblChooseDate.frame.origin.y + lblChooseDate.frame.height + 8
        btnDatePick.addTarget(self, action: #selector(self.chooseDate(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btnDatePick.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
        scrView.addSubview(btnDatePick)
        
        for var i in (0 ..< 2)
        {
            let lbl = UILabel(frame : CGRect(x: sideAlignment, y: btnDatePick.frame.origin.y + btnDatePick.frame.height + 30.0 + 40.0 * CGFloat(i), width: 100, height: 100))
            lbl.text = titles[i]
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Medium", size: 16)
            lbl.sizeToFit()
            lbl.alpha = 0.7
            scrView.addSubview(lbl)
            
            let lblResult = UILabel(frame : CGRect(x: sideAlignment, y: btnDatePick.frame.origin.y + btnDatePick.frame.height + 30.0 + 40.0 * CGFloat(i), width: self.frame.width - sideAlignment * 2, height: 100))
            lblResult.text = ""
            lblResult.textColor = UIColor.black
            lblResult.font = UIFont(name: "SFUIText-Medium", size: 16)
            lblResult.textAlignment = .right
            lblResult.sizeToFit()
            lblResult.frame.origin.x = self.frame.width - sideAlignment - lblResult.frame.width
            lblResults.append(lblResult)
            scrView.addSubview(lblResult)
        }
        
        setupTable()
        scrView.contentSize.height = 360 + 360
        if (scrView.contentSize.height < scrView.frame.height)
        {
            scrView.contentSize.height = scrView.frame.height + 1
        }
        
        btnOvForm.setTitle("Make A Form", for: .normal)
        btnOvForm.setTitleColor(UIColor.black, for: .normal)
        btnOvForm.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 20)
        btnOvForm.frame = CGRect(x: 0, y: self.frame.height - 80, width: self.frame.width, height: 80)
        btnOvForm.backgroundColor = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        btnOvForm.addTarget(self, action: #selector(self.overtimeLeaveForm(_:)), for: .touchUpInside)
        btnOvForm.layer.addBorder(.top, color: UIColor(red: 230/255, green: 230/255, blue: 230/255, alpha : 1), thickness: 1)
        self.addSubview(btnOvForm)
        
        let btnView = UIView(frame : CGRect(x: sideAlignment, y: 0, width: 15, height: 15))
        btnView.backgroundColor = UIColor(red: 255/255, green: 174/255, blue: 14/255, alpha: 1)
        btnView.layer.cornerRadius = btnView.frame.height / 2
        btnView.center.y = 40
        btnOvForm.addSubview(btnView)
        
    }
    
    //API Post----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func api_Post()
    {
        let auth = vc.userInform.auth_token
        self.vc.loadingLabelUp(label: "Requesting..")
        
        let parameters = [
            "month" : "\(curMonthNum)",
            "year" : "\(curYearNum)"
        ]
        
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)/overtimes?page=1&auth_token=\(auth)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
            .validate()
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    print(JSON)
                    self.vc.loadingView.succeeded()
                    self.autoGoBackReady = true
                    self.JSON = JSON as! AnyObject
                    self.setResults(JSON : JSON as! [NSDictionary])
                    
                case .failure(let error) :
                    print(" ERROR IS \(error)")
                }
        }
        
    }
    
    func setResults(JSON : [NSDictionary])
    {
        setupTable()
        print("Table setup")
        
        for var i in (0 ..< JSON.count)
        {
            let day = JSON[i]
            print("rows called")
            
            let lbl = UILabel(frame : CGRect(x: cellStart[0] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[0], height: 80))
            lbl.text = day["date"] as! String
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl.sizeToFit()
            lbl.alpha = 0.7
            tableView.addSubview(lbl)
            
            let lbl2 = UILabel(frame : CGRect(x: cellStart[1] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[1], height: 80))
            lbl2.text = day["start_time"] as! String
            lbl2.textColor = UIColor.black
            lbl2.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl2.sizeToFit()
            lbl2.alpha = 0.7
            tableView.addSubview(lbl2)
            
            let lbl3 = UILabel(frame : CGRect(x: cellStart[2] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[2], height: 80))
            lbl3.text = day["end_time"] as! String
            lbl3.textColor = UIColor.black
            lbl3.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl3.sizeToFit()
            lbl3.alpha = 0.7
            tableView.addSubview(lbl3)
            
            let lbl4 = UILabel(frame : CGRect(x: cellStart[3] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[3], height: 80))
            lbl4.text = "\(day["ot_hours"] as! String) hours"
            lbl4.textColor = UIColor.black
            lbl4.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl4.sizeToFit()
            lbl4.alpha = 0.7
            tableView.addSubview(lbl4)
            
            let lbl5 = UILabel(frame : CGRect(x: cellStart[4] + sideAlignment, y: 40 * CGFloat(i) + 80, width: cellWidths[4], height: 80))
            lbl5.text = "\(day["evaluated_hours"] as! String) hours"
            lbl5.textColor = UIColor.black
            lbl5.font = UIFont(name: "SFUIText-Medium", size: 15)
            lbl5.sizeToFit()
            lbl5.alpha = 0.7
            tableView.addSubview(lbl5)
            
            tableView.frame.size.height = lbl.frame.origin.y + lbl.frame.size.height + 20
            
        }
        
        scrView.contentSize.height = tableView.frame.origin.y + tableView.frame.height + 60
        
        if (scrView.contentSize.height < scrView.frame.height)
        {
            scrView.contentSize.height = scrView.frame.height + 1
        }
        
    }
    
    //resets some UI properties and variables to call the API again
    func resetProperties()
    {
        for ss in tableView.subviews
        {
            ss.removeFromSuperview()
        }
//        }
//        for var i in (0 ..< 5)
//        {
//            btnWeeks[i].alpha = 0.4
//        }
    }
    
    
    //Change the date on the button and request the api
    func changeButtonDate()
    {
        btnDatePick.setTitle("\(curMonth) \(curYear)", for: .normal)
        btnDatePick.sizeToFit()
        btnDatePick.frame.size.width = btnDatePick.frame.width + 20
        btnDatePick.frame.size.height = btnDatePick.frame.height + 5
        api_Post()
    }
    
    //Choose Month and Year---------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func chooseDate(_ btn : UIButton)
    {
        vc.monthYearPicker = MonthYearPicker()
        vc.monthYearPicker.frame = vc.view.frame
        vc.monthYearPicker.initialize(vc, parentView : "OvertimeView")
        vc.view.addSubview(vc.monthYearPicker)
    }
    
    
    //Setup Collection View---------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func setupTable()
    {
        resetProperties()
        tableView.frame = CGRect(x: 0, y: 340, width: self.frame.width, height: 360)
        
        let borderView = UIView(frame: CGRect(x: 0, y: 340, width: self.frame.width, height: 1))
        borderView.backgroundColor = UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1)
        scrView.addSubview(borderView)
        
        for var i in (0 ..< 5)
        {
            let lbl = UILabel(frame : CGRect(x: cellStart[i] + sideAlignment, y: 30, width: cellWidths[i], height: 80))
            lbl.text = cellTitles[i]
            lbl.textColor = UIColor.black
            lbl.font = UIFont(name: "SFUIText-Bold", size: 16)
            lbl.sizeToFit()
            tableView.addSubview(lbl)
            tableView.contentSize.width = lbl.frame.origin.x + lbl.frame.width + sideAlignment
            tableView.frame.size.height = lbl.frame.origin.y + lbl.frame.size.height + 20
        }
        
        scrView.addSubview(tableView)
    }
    
    //Go Back to Home Screen--------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func goBackHome(_ btn : UIButton)
    {
        vc.homeView.viewBtns[3].alpha = 1
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.vc.homeView.btnBorders[3].alpha = 1
            
        }, completion: {finish in
            self.removeFromSuperview()
        })
        self.vc.homeView.comeBack(delay: 0.15)
    }
    
    //Scroll View Delegate Methods--------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let scrOffset = scrView.contentOffset.y
        
        if (scrOffset > previousOffsetY)
        {
            forceCancel = true
        }
        else if (scrOffset < previousOffsetY)
        {
            forceCancel = false
        }
        
        previousOffsetY = scrOffset
        
        if (scrOffset >= 110)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 1
                self.headerCover.frame.origin.y = 0
                
            }, completion: nil)
        }
        else
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.headerCover.alpha = 0
                self.headerCover.frame.origin.y = -20
                
            }, completion: nil)
        }
        
        if (scrOffset <= -0 && (goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            self.transform = CGAffineTransform(scaleX: 1 + ((scrOffset + 0) * 0.0015), y: 1 + ((scrOffset + 0) * 0.0015))
            self.center.y = self.vc.view.frame.height / 2 - (scrOffset + 0) * 0.5
        }
        else if((goBackState == "state1" || goBackState == "state2") && autoGoBackReady)
        {
            UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.transform = CGAffineTransform(scaleX: 1, y: 1)
                self.center.y = self.vc.view.frame.height / 2
                
            }, completion: nil)
        }
        
        if (scrOffset <= -80 && goBackState == "state1" && autoGoBackReady)
        {
            goBackState = "state2"
            vc.viewSwitchLabel.showFromAbove(text: "Release to go back")
        }
        else if (scrOffset >= -80 && goBackState == "state2" && autoGoBackReady)
        {
            goBackState = "state1"
            vc.viewSwitchLabel.hide()
        }
        
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if (goBackState == "state2" && !forceCancel && autoGoBackReady)
        {
            goBackState = "state3"
            vc.viewSwitchLabel.hide()
            UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                
                self.alpha = 0
                self.transform = CGAffineTransform(scaleX: 0.4, y: 0.4)
                self.frame.origin.y = self.vc.view.frame.height
                
            }, completion: {finish in
                
                UIView.animate(withDuration: 0.2, animations: {
                    self.vc.homeView.viewBtns[3].alpha = 1
                })
                self.vc.homeView.comeBack(delay : 0)
                self.vc.homeView.btnBorders[3].alpha = 1
                self.removeFromSuperview()
                
            })
        }
    }

    
    //Request Overtime Form------------------------------------------------------------
    //---------------------------------------------------------------------------------
    //---------------------------------------------------------------------------------
    
    func overtimeLeaveForm(_ btn : UIButton)
    {
        vc.overtimeFormView = OvertimeFormView()
        vc.overtimeFormView.frame = vc.view.frame
        vc.overtimeFormView.initialize(self.vc)
        vc.view.addSubview(vc.overtimeFormView)
        
        let blackCover = UIView(frame : self.frame)
        blackCover.backgroundColor = UIColor.black
        blackCover.alpha = 0
        self.addSubview(blackCover)
        
        vc.overtimeFormView.frame.origin.y = self.frame.height
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = -50
            blackCover.alpha = 0.5
            self.vc.overtimeFormView.frame.origin.y = 0
            
            }, completion: {finish in
                blackCover.removeFromSuperview()
        })
    }
    
    
    func comeBack()
    {
        let blackCover = UIView(frame : self.frame)
        blackCover.backgroundColor = UIColor.black
        blackCover.alpha = 0.5
        self.addSubview(blackCover)
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = 0
            blackCover.alpha = 0
            
            }, completion: {finish in
                blackCover.removeFromSuperview()
        })
    }


}
