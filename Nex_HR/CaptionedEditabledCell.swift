//
//  CaptionedEditabledCell.swift
//  Nex_HR
//
//  Created by Daud on 11/21/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit
import Alamofire
import Foundation
import SwiftyJSON

class CaptionedEditabledCell: UITableViewCell {
    
    var vc : ViewController!
    var parentView = ""
    
    let lbl = UILabel()
    let btnEdit = UIButton()
    
    var txts = [UITextField]()
    var btns = [UIButton]()
    
    var emerName = [String]()
    var emerPhone = [String]()
    var phoneList = [String]()
//    var phone : NSDictionary!
    
    func initialize(vc : ViewController, parentView : String, lbl1 : String, list : [String])
    {
        self.phoneList = vc.userInform.phone
        self.emerName = vc.userInform.emerName
        self.emerPhone = vc.userInform.emerPhone
        setupInterface(vc : vc, parentView: parentView, lbl1 : lbl1)
        
        for var i in (0 ..< list.count)
        {
            let txt = UITextField(frame: CGRect(x: 20, y: 35 + ( i * 50), width: Int(self.frame.width) - 60, height: 40))
            txt.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
            txt.font = UIFont(name: "SFUIText-Medium", size: 18)
            txt.textColor = UIColor.black
            txt.text = list[i]
            txt.isUserInteractionEnabled = false
            self.addSubview(txt)
            txts.append(txt)
            
            let btnDel = UIButton()
            btnDel.setImage(UIImage(named : "btnDel.png"), for: .normal)
            btnDel.frame = CGRect(x: self.frame.width - 20, y: 35, width: 20, height: 20)
            btnDel.center.y = txt.center.y
            btnDel.tag = i
            btnDel.addTarget(self, action: #selector(self.deleteNumber(_:)), for: .touchUpInside)
            self.addSubview(btnDel)
            btns.append(btnDel)
        }
    }
    
    func initialize(vc : ViewController, parentView : String, lbl1 : String, emerName : [String], emerPhone : [String])
    {
        self.phoneList = vc.userInform.phone
        self.emerName = vc.userInform.emerName
        self.emerPhone = vc.userInform.emerPhone
        setupInterface(vc : vc, parentView: parentView, lbl1 : lbl1)
        
        for var i in (0 ..< emerName.count)
        {
            let txt = UITextField(frame: CGRect(x: 20, y: 35 + ( i * 50), width: Int(self.frame.width) - 60, height: 40))
            txt.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
            txt.font = UIFont(name: "SFUIText-Medium", size: 18)
            txt.textColor = UIColor.black
            txt.text = "\(emerName[i]) : \(emerPhone[i])"
            txt.isUserInteractionEnabled = false
            self.addSubview(txt)
            txts.append(txt)
            
            let btnDel = UIButton()
            btnDel.setImage(UIImage(named : "btnDel.png"), for: .normal)
            btnDel.frame = CGRect(x: self.frame.width - 20, y: 35, width: 20, height: 20)
            btnDel.center.y = txt.center.y
            btnDel.tag = i
            btnDel.addTarget(self, action: #selector(self.deleteNumber(_:)), for: .touchUpInside)
            self.addSubview(btnDel)
            btns.append(btnDel)
        }
    }
    
    func setupInterface(vc : ViewController, parentView : String, lbl1 : String)
    {
        for cc in self.subviews
        {
            cc.removeFromSuperview()
        }
        txts.removeAll()
        btns.removeAll()
        
        self.vc = vc
        self.parentView = parentView
        
        lbl.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 30)
        lbl.font = UIFont(name: "SFUIText-Medium", size: 18)
        lbl.textColor = UIColor.black
        lbl.alpha = 0.5
        lbl.text = lbl1
        self.addSubview(lbl)
        
        btnEdit.setTitle("Add", for: .normal)
        btnEdit.setTitleColor(UIColor(red: 18 / 255 , green : 158 / 255 , blue : 229 / 255, alpha : 1), for: .normal)
        btnEdit.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 16)
        btnEdit.sizeToFit()
        btnEdit.center.y = lbl.center.y
        btnEdit.frame.origin.x = self.frame.width - btnEdit.frame.width
        btnEdit.addTarget(self, action: #selector(self.addElement(_:)), for: .touchUpInside)
        self.addSubview(btnEdit)
    }
    
    func deleteNumber(_ btn : UIButton)
    {
        let startIndex = btn.tag
        
        btn.alpha = 0.5
        btn.isUserInteractionEnabled = false
        self.API_Update(startIndex : startIndex, btn : btn)
    }
    
    func API_Update(startIndex : Int, btn : UIButton)
    {
        var tempPhone = self.phoneList
        var tempEmerName = self.emerName
        var tempPhoneName = self.emerPhone
        
        if (self.parentView == "Phone")
        {
            tempPhone.remove(at: startIndex)
        }
        else if(self.parentView == "Emergency")
        {
            tempEmerName.remove(at: startIndex)
            tempPhoneName.remove(at: startIndex)
        }
        
        vc.loadingLabelUp(label : "Updating Info..")
        
        let dd = "Value"
        
        var tempEp : [[String : String]] = []
        
        for var i in(0 ..< tempEmerName.count)
        {
            let ep : [String : String] = [
                "name" : tempEmerName[i],
                "phone" : tempPhoneName[i],
                "relationship" : ""
            ]
            tempEp.append(ep)
        }
        
        let uv : [String: Any] = [
            "address" : vc.userInform.address,
            "phone" : tempPhone,
            "emergency_phone" : tempEp
        ]
        
        //Serialize the String : Any into text-------------------
        let data = try! JSONSerialization.data(withJSONObject: uv, options: .prettyPrinted)
        
        let parameters = [
            "update_values" : String(data: data, encoding: .utf8)!
        ]
        
        print(parameters)
        
        Alamofire.request("\(BASE_URL)employees/\(vc.userInform.id)?auth_token=\(vc.userInform.auth_token)", method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: ["Content-Type" : "application/json"])
            .validate()
            .validate(contentType: ["application/json"])
            .responseJSON { response in
                switch(response.result)
                {
                case .success(let JSON) :
                    
                    UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
                        
                        self.btns[startIndex].alpha = 0
                        self.txts[startIndex].frame.origin.x = -self.frame.width
                        self.txts[startIndex].alpha = 0
                        
                    }, completion: {finish in
                        
                        self.btns[startIndex].removeFromSuperview()
                        self.txts[startIndex].removeFromSuperview()
                        
                        self.txts.remove(at: startIndex)
                        self.btns.remove(at: startIndex)
                        self.phoneList.remove(at: startIndex)
                        
                        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0, options: [], animations: {
                            
                            for var i in (0 ..< self.phoneList.count)
                            {
                                self.txts[i].frame.origin.y = CGFloat(35 + (i * 50))
                                self.btns[i].center.y = self.txts[i].center.y
                                self.btns[i].tag = i
                            }
                            
                        }, completion: {finish in
                            
                            if (self.parentView == "Phone")
                            {
                                self.phoneList = tempPhone
                                self.vc.userInform.phone = self.phoneList
                            }
                            else if(self.parentView == "Emergency")
                            {
                                self.emerName = tempEmerName
                                self.emerPhone = tempPhoneName
                                self.vc.userInform.emerName = self.emerName
                                self.vc.userInform.emerPhone = self.emerPhone
                            }
                            self.vc.profileView.updateInformation()
                            
                            self.vc.profileView.tabView.beginUpdates()
                            self.vc.profileView.tabView.endUpdates()
                            
                        })
                        
                        
                    })
                    self.vc.loadingView.succeeded()
                    
                case .failure(let error) :
                    self.vc.loadingView.failed()
                    btn.alpha = 1
                    btn.isUserInteractionEnabled = true
                    
                    print(" UPDATING ERROR IS \(error)")
                }
        }
        
        
    }
    
    func addElement(_ btn : UIButton)
    {
        vc.addPhoneView = AddPhone()
        vc.addPhoneView.frame = vc.view.frame
        vc.addPhoneView.initialize(self.vc, parentView: parentView)
        self.vc.view.addSubview(vc.addPhoneView)
    }

}
