import Foundation
import UIKit

class User_Inform
{
    var auth_token = ""
    
    var id = ""
    var name = ""
    var email = ""
    var department = ""
    var eCode = ""
    var bankAcc = ""
    var position = ""
    var gender = ""
    var doB = ""
    var nrc = ""
    var address = ""
    var phone = [String]()
//    var emerContact = [String]()
    var emerName = [String]()
    var emerPhone = [String]()
    var serLength = ""
    var salary = ""
    
    var months : [String] = ["January","February","March","April","May","June","July","August","September","October","November","December"]
    var years : [String] = []
    var monthsNum : [Int] = [1,2,3,4,5,6,7,8,9,10,11,12]
    var yearsNum : [Int] = []
    var curMonth : String = ""
    var curYear : String = ""
    var curMonthNum : Int = 0
    var curYearNum : Int = 0
    var curMonthIndex : Int = 0
    var curYearIndex : Int = 0
    var curDayNum : Int = 0
    
    var leaveTypes = [String]()
    var leaveTypesIDs = [String]()
    var leaveColors = [UIColor(),UIColor(),UIColor(),UIColor(),UIColor()]
    var statusTypes = [String]()
    var ApprovedByNames = [String]()
    var ApprovedByJobs = [String]()
    var ApprovedByIDs = [String]()
    var ReportToNames = [String]()
    var ReportToJobs = [String]()
    var ReportToIDs = [String]()
    
}
