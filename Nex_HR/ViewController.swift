//
//  ViewController.swift
//  Nex_HR
//
//  Created by Daud on 9/28/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Classes---------------------------
    
    var homeView : HomeView!
    var profileView : ProfileView!
    var leaveView : LeaveView!
    var payRollView : PayRollView!
    var leaveFormView : LeaveFormView!
    var monthYearPicker : MonthYearPicker!
    var attendenceView : AttendenceView!
    var overtimeView : OvertimeView!
    var overtimeFormView : OvertimeFormView!
    var signInView : SignInView!
    var dayYearPicker : DayYearPicker!
    var timePicker : TimePicker!
    var selectorView : SelectorView!
    var addPhoneView : AddPhone!
    var employeeHandBook : EmployeeHandbook!
    var employeeMenu : EmployeeMenu!
    var checkIn : CheckIn!
    var manualView : ManualView!
    
    var userInform : User_Inform!
    var statusBarHidden = true
    
    var loadingView : LoadingView!
    let viewSwitchLabel = ViewSwitchLabel()
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 240/255, green: 240/255, blue: 240/255, alpha: 1)
        self.view.layer.cornerRadius = 4
        self.view.clipsToBounds = true
        
        userInform = User_Inform()
        initializeDate()
        
        checkSignedIn()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return statusBarHidden
    }
    
    //Initialize date information and store them in userInform so that every class can use it------------------
    func initializeDate()
    {
        var months : [String] = ["January","February","March","April","May","June","July","August","September","October","November","December"]
        
        let date = Date()
        let calendar = Calendar.current
        let component = calendar.dateComponents([.year,.month,.day], from: date as Date)
        
        let year =  component.year
        let month = component.month
        let day = component.day
        
        userInform.curMonth = "\(months[month!-1])"
        userInform.curYear = "\(year!)"
        
        userInform.curMonthNum = month!
        userInform.curYearNum = year!
        
        for var i in (2014 ..< year! + 1)
        {
            userInform.yearsNum.append(i)
            userInform.years.append("\(i)")
        }
        
        for var i in (0 ..< 12)
        {
            if (userInform.curMonth == userInform.months[i])
            {
                userInform.curMonthIndex = i
            }
        }
        userInform.curYearIndex = userInform.years.count - 1
        userInform.curDayNum = day!
        
        viewSwitchLabel.initializer(vc: self)
        view.addSubview(viewSwitchLabel)
    }
    
    //Check if user is signed in, if false, will show sign in view-----------------------
    func checkSignedIn()
    {
        signInView = SignInView()
        signInView.frame = self.view.frame
        
        let userDefault = UserDefaults.standard
        
        if (userDefault.bool(forKey: "signedIn") == false)
        {
            signInView.initialize(vc: self)
        }
        else
        {
            signInView.automaticSignIn(vc: self)
        }
        
        self.view.addSubview(signInView)
        
        signInView.frame.origin.y = self.view.frame.height
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: [], animations: {
            
            self.view.backgroundColor = UIColor.white
            self.signInView.frame.origin.y = 0
            
        }, completion: nil)
        
        
        
    }
    
    //Initialize and bring up loading label--------------------
    func loadingLabelUp(label : String)
    {
        if (loadingView != nil)
        {
            loadingView.removeFromSuperview()
        }
        loadingView = LoadingView()
        loadingView.initialize(vc: self, label : label)
        self.view.addSubview(loadingView)
    }
    
    //Button Press Fade Functions----------------------
    func btnPressed(_ btn : UIButton)
    {
        btn.alpha = 0.5
    }
    func btnReleased(_ btn : UIButton)
    {
        btn.alpha = 0.5
        UIView.animate(withDuration: 0.2, animations: {
            btn.alpha = 1
        })
    }
    
}

// Extension of CALayer for top,bottom,left and right borders
extension CALayer {
    
    func addBorder(_ edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect(x: 0, y: self.frame.height - thickness, width: self.frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect(x: 0, y: 0, width: thickness, height: self.frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect(x: self.frame.width - thickness,y: 0, width: thickness, height: self.frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
    }
}

extension UIColor {
    
    convenience init(hexStr : String, alpha : CGFloat)
    {
        var cString : String = hexStr.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            self.init(red : 0, green : 0, blue : 0, alpha : alpha)
        }
        else
        {
            var rgbValue:UInt32 = 0
            Scanner(string: cString).scanHexInt32(&rgbValue)
            
            self.init(red : CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0, green : CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0, blue : CGFloat(rgbValue & 0x0000FF) / 255.0, alpha : alpha)
        }
        
        
    }
    
}

