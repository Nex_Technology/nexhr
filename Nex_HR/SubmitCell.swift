//
//  SubmitCell.swift
//  Nex_HR
//
//  Created by Daud on 11/3/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class SubmitCell: UITableViewCell {

    let btnSubmit = UIButton()
    
    var vc : ViewController!
    var parentClass = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func initialize(vc : ViewController, parentClass : String, cellNum : Int)
    {
        self.vc = vc
        self.parentClass = parentClass
        
        self.frame.size.height = 140
        btnSubmit.setTitle("Submit Form", for: .normal)
        btnSubmit.setTitleColor(UIColor.white, for: .normal)
        btnSubmit.titleLabel?.font = UIFont(name: "SFUIText-Medium", size: 18)
        btnSubmit.titleLabel?.textAlignment = .center
        btnSubmit.sizeToFit()
        btnSubmit.frame = CGRect(x: 0, y: 60, width: btnSubmit.frame.width + 40, height: btnSubmit.frame.height + 12)
        btnSubmit.backgroundColor = UIColor(red: 0, green: 204/255, blue: 126/255, alpha: 1)
        btnSubmit.layer.cornerRadius = btnSubmit.frame.height / 2
        btnSubmit.center.x = self.frame.width / 2
        btnSubmit.addTarget(self, action: #selector(self.submitForm(_:)), for: .touchUpInside)
        btnSubmit.addTarget(vc, action: #selector(vc.btnPressed(_:)), for: .touchDown)
        btnSubmit.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchUpInside)
        btnSubmit.addTarget(vc, action: #selector(vc.btnReleased(_:)), for: .touchDragOutside)
        self.addSubview(btnSubmit)
    }
    
    func submitForm(_ btn : UIButton)
    {
        print("pressed")
        if (parentClass == "leaveView")
        {
            vc.leaveFormView.submitForm()
        }
        else if (parentClass == "overtimeView")
        {
            vc.overtimeFormView.submitForm()
        }
        else if (parentClass == "manualView")
        {
            print("pressed")
            vc.manualView.submitForm()
        }
    }

}
