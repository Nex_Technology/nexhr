//
//  LeaveFormView.swift
//  Nex_HR
//
//  Created by Daud on 10/3/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import Alamofire
import UIKit

class LeaveFormView: UIView, UITableViewDelegate, UITableViewDataSource {
    
    var vc : ViewController!
    var endDateCell : DatePickerCell!
    
    var sideAlignment : CGFloat = 20
    var scrollIndex = 0
    
    let headerCover = UIView()
//    let scrView = UIScrollView()
    let tabView = UITableView()
    
    
    var txtColor : [UIColor] = [UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1),UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)]
    
    let lblInit : [String] = ["Is Half Day?", "Start Date", "End Date", "Leave Type","Image","Approved By", "Report To","Reason"]
    
    var curMonth = ["October","October"]
    var curYear = ["2016","2016"]
    var curYearNum = [0,0]
    var curMonthNum = [0,0]
    var curMonthIndex = [0,0]
    var curYearIndex = [0,0]
    var curDayNum = [0,0]
    var curDayIndex = [0,0]
//    
//    var curHourIndex = [0,0]
//    var curMinuteIndex = [0,0]
//    var curTimeIndex = [0,0]
//    
//    var curHour = ["9","9"]
//    var curMinute = ["30","30"]
//    var curTime = ["am","am"]
    
    var isHalfDay = "0"
    var halfDayType = 0
    
    var curLeaveType = "Casual Leave"
    var curLeaveTypeId = "0"
    var curApprovedBy = "SomeOne"
    var curReportTo = "SomeOne"
    var curApprovedById = ""
    var curReportToId = ""
    var curReason = "Reason"
    var curReasonColor = UIColor(red: 203/255, green: 203/255, blue: 203/255, alpha: 1)
    var choosenImage = UIImage()
    
    var leaveTypes = ["Casual Leave","Medicine Leave","Annual Leave"]
    var leaveTypesId = [String]()
    var approvedBys = [String]()
    var approvedByIDs = [String]()
    var reportTos = [String]()
    var reportToIDs = [String]()
    
    
//    var lbl2 : [String] = ["Is Half Day?", "Start Date", "End Date", "Leave Type","Image","Approved By", "Report To","Y U gonna leave?"]
    
    func initialize(_ vc : ViewController!)
    {
        self.vc = vc
        self.backgroundColor = UIColor.white
        
        curMonthNum = [vc.userInform.curMonthNum,vc.userInform.curMonthNum]
        curYearNum = [vc.userInform.curYearNum,vc.userInform.curYearNum]
        curMonth = [vc.userInform.curMonth,vc.userInform.curMonth]
        curYear = [vc.userInform.curYear,vc.userInform.curYear]
        curMonthIndex = [vc.userInform.curMonthIndex,vc.userInform.curMonthIndex]
        curYearIndex = [vc.userInform.curYearIndex,vc.userInform.curYearIndex]
        curDayNum = [vc.userInform.curDayNum,vc.userInform.curDayNum]
        curDayIndex = [vc.userInform.curDayNum - 1, vc.userInform.curDayNum - 1]
        
        leaveTypes = vc.userInform.leaveTypes
        leaveTypesId = vc.userInform.leaveTypesIDs
        
        for var i in (0 ..< vc.userInform.ApprovedByNames.count)
        {
            approvedBys.append("\(vc.userInform.ApprovedByNames[i]) (\(vc.userInform.ApprovedByJobs[i]))")
        }
        approvedByIDs = vc.userInform.ApprovedByIDs
        
        for var i in (0 ..< vc.userInform.ReportToNames.count)
        {
            reportTos.append("\(vc.userInform.ReportToNames[i]) (\(vc.userInform.ReportToJobs[i]))")
        }
        reportToIDs = vc.userInform.ReportToIDs
        
        curLeaveType = leaveTypes[0]
        curApprovedById = leaveTypesId[0]
        curApprovedBy = approvedBys[0]
        curApprovedById = approvedByIDs[0]
        curReportTo = reportTos[0]
        curReportToId = reportToIDs[0]
        
        tabView.dataSource = self
        tabView.delegate = self
        tabView.register(SubmitCell.self, forCellReuseIdentifier: "submitCell")
        tabView.register(TextViewCell.self, forCellReuseIdentifier: "textViewCell")
        tabView.register(DatePickerCell.self, forCellReuseIdentifier: "datePickerCell")
        tabView.register(AttachmentCell.self, forCellReuseIdentifier: "attachmentCell")
        tabView.register(SelecterCell.self, forCellReuseIdentifier: "selecterCell")
        tabView.register(HalfDayCell.self, forCellReuseIdentifier: "halfDayCell")
        tabView.frame = CGRect(x: sideAlignment, y: 120, width: self.frame.width - sideAlignment * 2, height: self.frame.height - 120)
        tabView.separatorColor = UIColor.clear
        tabView.showsVerticalScrollIndicator = false
        tabView.delaysContentTouches = false
        self.addSubview(tabView)
        
        headerCover.frame = CGRect(x: 0, y: 0, width: self.frame.width, height: 100)
        headerCover.backgroundColor = UIColor.white
        headerCover.layer.addBorder(.bottom, color: UIColor(red: 230/255, green : 230/255, blue : 230/255, alpha : 1), thickness: 1)
        headerCover.alpha = 1
        self.addSubview(headerCover)
        
        let btnCancle = UIButton(frame: CGRect(x: sideAlignment, y: 0, width: 100, height: 100))
        btnCancle.setTitle("Cancel", for: .normal)
        btnCancle.setTitleColor(UIColor(red : 0/255, green : 175/255, blue : 255/255, alpha : 1), for: .normal)
        btnCancle.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 18)
        btnCancle.sizeToFit()
        btnCancle.frame.origin.x = self.frame.width - btnCancle.frame.width - sideAlignment
        btnCancle.center.y = 50
        headerCover.addSubview(btnCancle)
        btnCancle.addTarget(self, action: #selector(self.goBack(_:)), for: .touchUpInside)
        
        let lblLeave = UILabel()
        lblLeave.text = "Leave"
        lblLeave.textColor = UIColor.black
        lblLeave.font = UIFont(name: "SFUIText-Bold", size: 25)
        lblLeave.sizeToFit()
        lblLeave.frame.origin.x = sideAlignment
        lblLeave.center.y = 50
        headerCover.addSubview(lblLeave)
        
        let lblDate = UILabel()
        lblDate.text = "2.9.2016"
        lblDate.textColor = UIColor.black
        lblDate.font = UIFont(name: "SFUIText-Regular", size: 15)
        lblDate.sizeToFit()
        lblDate.frame.origin.x = sideAlignment
        lblDate.center.y = 80
        headerCover.addSubview(lblDate)
        
        //Keyboard notification to listen to keyboard events
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillShow), name: .UIKeyboardWillShow, object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: .UIKeyboardWillHide, object: nil)
    }
    
    //to get the keyboard height--------------------
    func keyboardWillShow(notification: NSNotification)
    {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue{
            let keyboardHeight = keyboardSize.height
            
            tabView.frame.size.height = frame.height - tabView.frame.origin.y - keyboardHeight
            UIView.animate(withDuration: 0, delay: 0.3, options: [], animations: {
                
                
            }, completion: {finish in
                
                self.tabView.scrollToRow(at: NSIndexPath(row: self.scrollIndex, section: 0) as IndexPath, at: .middle, animated: true)
            })
            
        }
    }
    
    //Scoll the tableCell to middle when textField is focused-----------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func keyboardWillHide(notification : NSNotification)
    {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0, options: [], animations: {
            
            self.tabView.frame.size.height = self.frame.height - self.tabView.frame.origin.y
            
        }, completion: nil)
    }
    
    //Go back-----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    func goBack(_ btn : UIButton!)
    {
        self.endEditing(true)
        vc.leaveView.comeBack()
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.frame.height
            
        }, completion: {finish in
            
            self.removeFromSuperview()
            
        })
        
    }
    
    //Go back-----------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func submitForm()
    {
        
        vc.loadingLabelUp(label: "Sending..")
        var endDate = ""
        if (isHalfDay == "0")
        {
            endDate = "\(curYear[1])-\(curMonth[1])-\(curDayNum[1])"
        }
        else
        {
            endDate = "\(curYear[0])-\(curMonth[0])-\(curDayNum[0])"
        }
        
        let parameters = [
            "employee_id" : "\(vc.userInform.id)",
            "requested_date" : "\(vc.userInform.curYear)-\(vc.userInform.curMonthNum)-\(vc.userInform.curDayNum)",
            "start_date" : "2017-7-21",
            "end_date" : "2017-7-21",
            "is_half_day" : "\(isHalfDay)",
            "half_day_type" : "\(halfDayType)",
            "leave_type_id" : "\(curLeaveTypeId)",
            "reason" : "\(curReason)",
            "first_approved_by_id" : curApprovedById,
            "second_approved_by_id" : curReportToId
        ]
//        let parameters = [
//            "employee_id" : "1",
//            "requested_date" : "2017-6-21",
//            "start_date" : "2017-6-21",
//            "end_date" : "2017-6-21",
//            "is_half_day" : "false",
//            "half_day_type" : "0",
//            "leave_type_id" : "1",
//            "reason" : "gg",
//            "first_approved_by_id" : "15",
//            "second_approved_by_id" : "7"
//        ]
        
        let image = choosenImage
        self.endEditing(true)
        
        print("parameters are \(parameters)")
//        Alamofire.request("\(BASE_URL)leaves?auth_token=\(vc.userInform.auth_token)", method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: nil)
//            .validate()
//            .responseJSON { response in
//                switch(response.result)
//                {
//                case .success(let JSON) :
//                    print(JSON)
//                    self.vc.loadingView.succeeded()
//                    
//                case .failure(let error) :
//                    print(" ERROR IS \(error)")
//                    self.vc.loadingView.failed()
//                }
//        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            
            multipartFormData.append(UIImageJPEGRepresentation(image, 0.6)!, withName: "attachment", fileName: "img.jpeg", mimeType: "image/jpeg")
            
            for (key, value) in parameters {
                multipartFormData.append(value.data(using: String.Encoding.utf8)!, withName: key)
            }
            
            },
            to: "\(BASE_URL)leaves?auth_token=\(vc.userInform.auth_token)",
            encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                    
                    
                    upload.responseJSON(completionHandler:
                        {response in
                            
                            print("response result = \(response.result)")
                            print("response data = \(response.data)")
                            
                            switch(response.result)
                            {
                            case .success(let JSON):
                                
                                print(JSON)
                                
                                self.vc.leaveView.comeBack()
                                self.vc.loadingView.succeeded()
                                
                                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
                                    
                                    self.frame.origin.y = self.frame.height
                                    
                                }, completion: {finish in
                                    
                                    self.removeFromSuperview()
                                    
                                })
                            
                            case .failure(let erorr):
                                print(erorr)
                                self.vc.loadingView.failed()
                            }
                            
                    }
                    )
                    
                case .failure(let encodingError):
                    print(encodingError)
                    self.vc.loadingView.failed()
                    
                }
        })
    
    }
    
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 9
    }
    
    
//    let lblInit : [String] = ["Is Half Day?", "Start Date", "End Date", "Leave Type","Image","Approved By", "Report To","Y U gonna leave?"]
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if (indexPath.row == 0) //HalfDayCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "halfDayCell", for: indexPath as IndexPath) as! HalfDayCell
            cell.initialize(lbl : lblInit[indexPath.row], halfDayType : "\(halfDayType)", vc : self.vc, parentView : "leaveView")
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 1 || indexPath.row == 2) //DatePickerCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "datePickerCell", for: indexPath as IndexPath) as! DatePickerCell
            
            
            if (indexPath.row == 2)
            {
                endDateCell = cell
                if (halfDayType != 0)
                {
                    endDateCell.contentView.alpha = 0.4
                    endDateCell.contentView.isUserInteractionEnabled = false
                    print("Hello")
                }
                else
                {
                    endDateCell.contentView.alpha = 1
                    endDateCell.contentView.isUserInteractionEnabled = true
                }
            }
            
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : "\(curYear[indexPath.row - 1]) \(curMonth[indexPath.row - 1]) \(curDayNum[indexPath.row - 1])", vc : self.vc, parentView : "leaveView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            
            return cell
        }
        else if (indexPath.row == 3) //SelectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : curLeaveType, vc : self.vc, parentView : "leaveView", cellNum : indexPath.row, leaveList : leaveTypes, label : "Choose Leave Type?", idList : leaveTypesId)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 4) //AttachmentCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "attachmentCell", for: indexPath as IndexPath) as! AttachmentCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : "choose", vc : self.vc, parentView : "leaveView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 5) //SelectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : curApprovedBy, vc : self.vc, parentView : "leaveView", cellNum : indexPath.row, leaveList : approvedBys, label : "Who Approved You?", idList : approvedByIDs)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 6) //SelectorCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "selecterCell", for: indexPath as IndexPath) as! SelecterCell
            cell.initialize(lbl2 : lblInit[indexPath.row], btn2 : curReportTo, vc : self.vc, parentView : "leaveView", cellNum : indexPath.row, leaveList : reportTos, label : "Who Was Reported?", idList : reportToIDs)
            cell.selectionStyle = .none
            
            return cell
        }
        else if (indexPath.row == 7) //TextViewCell-----
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "textViewCell", for: indexPath as IndexPath) as! TextViewCell
            cell.initialize(vc : self.vc, lbl2 : curReason, parentClass : "leaveView", cellNum : indexPath.row, txtColor : curReasonColor)
            cell.selectionStyle = .none
            
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "submitCell", for: indexPath as IndexPath) as! SubmitCell
            cell.initialize(vc : self.vc, parentClass : "leaveView", cellNum : indexPath.row)
            cell.selectionStyle = .none
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if (indexPath.row == 8)
        {
            return 140
        }
        else if(indexPath.row == 7)
        {
            return 100
        }
        else
        {
            return 70
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.endEditing(true)
    }


}
