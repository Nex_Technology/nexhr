//
//  DayYearPicker.swift
//  Nex_HR
//
//  Created by Daud on 11/8/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class DayYearPicker: UIView, UITableViewDelegate, UITableViewDataSource, UIGestureRecognizerDelegate {
    
    var vc : ViewController!
    var monthCells = [PickerCell]()
    var yearCells = [PickerCell]()
    var dayCells = [PickerCell]()
    
    var pickerView = UIView()
    var dayTabView = UITableView()
    var monthTabView = UITableView()
    var yearTabView = UITableView()
    let btnChoose = UIButton(type : .system)
    
    var months = [String]()
    var years = [String]()
    var dayNum = [Int]()
    var dayCount = [31,28,31,30,31,30,31,31,30,31,30,31]
    var curMonthIndex = 0
    var curYearIndex = 0
    var curDayIndex = 0
    var parentView = ""
    var cellNum = 0
    
    var sideAlignment : CGFloat = 20
    
    func initialize(_ vc : ViewController, parentView : String, cellNum : Int)
    {
        self.vc = vc
        self.parentView = parentView
        self.cellNum = cellNum
        self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 0)
        
        let gesture = UITapGestureRecognizer(target: self, action: #selector(self.backgroundTapped(_:)))
        gesture.delegate = self
        self.addGestureRecognizer(gesture)
        
        months = vc.userInform.months
        years = vc.userInform.years
        
        if(parentView == "leaveView")
        {
            curMonthIndex = vc.leaveFormView.curMonthIndex[cellNum - 1]
            curYearIndex = vc.leaveFormView.curYearIndex[cellNum - 1]
            curDayIndex = vc.leaveFormView.curDayIndex[cellNum - 1]
        }
        else if(parentView == "overtimeView")
        {
            curMonthIndex = vc.overtimeFormView.curMonthIndex[cellNum]
            curYearIndex = vc.overtimeFormView.curYearIndex[cellNum]
            curDayIndex = vc.overtimeFormView.curDayIndex[cellNum]
        }
        
        pickerView.frame = CGRect(x: self.frame.width / 2, y: self.frame.height, width: self.frame.width - (sideAlignment * 2), height: 450)
        pickerView.backgroundColor = UIColor.clear
        self.addSubview(pickerView)
        
        let lblChoose = UILabel(frame : CGRect(x: 0, y: 20, width: 0, height: 0))
        lblChoose.text = "Choose year, month and day"
        lblChoose.textAlignment = .center
        lblChoose.textColor = UIColor.black
        lblChoose.alpha = 0.4
        lblChoose.font = UIFont(name: "SFUIText-Regular", size: 18)
        lblChoose.sizeToFit()
        pickerView.addSubview(lblChoose)
        
        btnChoose.setTitle("Choose", for: .normal)
        btnChoose.setTitleColor(UIColor.black, for: .normal)
        btnChoose.titleLabel?.font = UIFont(name: "SFUIText-Bold", size: 25)
        btnChoose.sizeToFit()
        btnChoose.frame.origin.y = 350
        btnChoose.frame.origin.x = 0
        btnChoose.addTarget(self, action: #selector(self.confirmDate(_:)), for: .touchUpInside)
        btnChoose.tintColor = UIColor.white
        pickerView.addSubview(btnChoose)
        
        
        yearTabView.frame = CGRect(x: 0, y: 60, width: pickerView.frame.width * 0.3, height: 250)
        yearTabView.register(PickerCell.self, forCellReuseIdentifier: "yearCell")
        yearTabView.delegate = self
        yearTabView.dataSource = self
        yearTabView.separatorColor = UIColor.clear
        yearTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(yearTabView)
        
        monthTabView.frame = CGRect(x: pickerView.frame.width * 0.3, y: 60, width: pickerView.frame.width * 0.45, height: 250)
        monthTabView.register(PickerCell.self, forCellReuseIdentifier: "monthCell")
        monthTabView.delegate = self
        monthTabView.dataSource = self
        monthTabView.separatorColor = UIColor.clear
        monthTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(monthTabView)
        
        dayTabView.frame = CGRect(x: pickerView.frame.width * 0.75 + 20, y: 60, width: pickerView.frame.width * 0.25, height: 250)
        dayTabView.register(PickerCell.self, forCellReuseIdentifier: "dayCell")
        dayTabView.delegate = self
        dayTabView.dataSource = self
        dayTabView.separatorColor = UIColor.clear
        dayTabView.showsVerticalScrollIndicator = false
        pickerView.addSubview(dayTabView)
        
        dayRefresh()
        
        pickerView.transform = CGAffineTransform(scaleX: 0, y: 0)
        pickerView.alpha = 0
        
        
        if(parentView == "leaveView")
        {
            vc.leaveView.alpha = 0
        }
        else if(parentView == "overtimeView")
        {
            vc.overtimeView.alpha = 0
        }
        
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.7, initialSpringVelocity: 0,options: [], animations: {
            
            self.backgroundColor = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
            self.pickerView.transform = CGAffineTransform(scaleX: 1, y: 1)
            self.pickerView.center = CGPoint(x : self.frame.width / 2 , y : self.frame.height / 2)
            self.pickerView.alpha = 1
            
            if(parentView == "leaveView")
            {
                self.vc.leaveFormView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            }
            else if(parentView == "overtimeView")
            {
                self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 0.94, y: 0.94)
            }
            
        }, completion: nil  )
        
        UIView.animate(withDuration: 0.5, delay: 0.2, options: [], animations: {
            
        }, completion: {finish in
            
            self.monthTabView.scrollToRow(at: NSIndexPath(row: self.curMonthIndex, section: 0) as IndexPath, at: .middle, animated: true)
            self.yearTabView.scrollToRow(at: NSIndexPath(row: self.curYearIndex, section: 0) as IndexPath, at: .middle, animated: true)
            self.dayTabView.scrollToRow(at: NSIndexPath(row: self.curDayIndex, section: 0) as IndexPath, at: .middle, animated: true)
            
        })
        
        
        
    }
    
    //Day Refresh-------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func dayRefresh()
    {
        let curYear = vc.userInform.yearsNum[curYearIndex]
        if (curYear % 4 == 0)
        {
            if (curYear % 100 == 0)
            {
                if (curYear % 400 == 0)
                {
                    dayCount[1] = 29
                }
                else
                {
                    dayCount[1] = 28
                }
            }
            else
            {
                dayCount[1] = 29
            }
        }
        else
        {
            dayCount[1] = 28
        }
        
        dayNum.removeAll()
        
        for var i in( 0 ..< dayCount[curMonthIndex] )
        {
            dayNum.append(i + 1)
        }
        
        dayTabView.reloadData()
        
    }
    
    
    //Table View Delegate Methods---------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if(tableView == monthTabView)
        {
            return 12
        }
        else if(tableView == yearTabView)
        {
            return years.count
        }
        else
        {
            return dayNum.count
        }
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(tableView == monthTabView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "monthCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize(months[indexPath.row],tag : indexPath.row, color : curMonthIndex == indexPath.row)
            monthCells.append(cell)
            cell.selectionStyle = .none
            return cell
        }
        else if(tableView == yearTabView)
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "yearCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize("\(years[indexPath.row])",tag : indexPath.row, color : curYearIndex == indexPath.row)
            yearCells.append(cell)
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "dayCell", for: indexPath as IndexPath) as! PickerCell
            cell.initialize("\(dayNum[indexPath.row])",tag : indexPath.row, color : curDayIndex == indexPath.row)
            dayCells.append(cell)
            cell.selectionStyle = .none
            return cell
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    // set current month and year when the table cells are tappped------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if(tableView == monthTabView)
        {
            for cc in monthCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curMonthIndex = indexPath.row
            dayRefresh()
            print(curMonthIndex)
        }
        else if(tableView == yearTabView)
        {
            for cc in yearCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curYearIndex = indexPath.row
            dayRefresh()
            print(curYearIndex)
        }
        else
        {
            for cc in dayCells
            {
                cc.lbl.textColor = UIColor.black
                if (cc.tag == indexPath.row)
                {
                    cc.lbl.textColor = UIColor(red: 0, green: 162/255, blue: 236/255, alpha: 1)
                }
            }
            curDayIndex = indexPath.row
            print(curDayIndex)
        }
    }
    
    //Choose Date-------------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func confirmDate(_ btn : UIButton)
    {
        if(parentView == "leaveView")
        {
            vc.leaveFormView.curMonthIndex[cellNum - 1] = curMonthIndex
            vc.leaveFormView.curYearIndex[cellNum - 1] = curYearIndex
            vc.leaveFormView.curDayIndex[cellNum - 1] = curDayIndex
            vc.leaveFormView.curMonth[cellNum - 1] = vc.userInform.months[curMonthIndex]
            vc.leaveFormView.curYear[cellNum - 1] = vc.userInform.years[curYearIndex]
            vc.leaveFormView.curMonthNum[cellNum - 1] = vc.userInform.monthsNum[curMonthIndex]
            vc.leaveFormView.curYearNum[cellNum - 1] = vc.userInform.yearsNum[curYearIndex]
            vc.leaveFormView.curDayNum[cellNum - 1] = dayNum[curDayIndex]
            vc.leaveFormView.tabView.reloadData()
        }
        if(parentView == "overtimeView")
        {
            vc.overtimeFormView.curMonthIndex[cellNum] = curMonthIndex
            vc.overtimeFormView.curYearIndex[cellNum] = curYearIndex
            vc.overtimeFormView.curDayIndex[cellNum] = curDayIndex
            vc.overtimeFormView.curMonth[cellNum] = vc.userInform.months[curMonthIndex]
            vc.overtimeFormView.curYear[cellNum] = vc.userInform.years[curYearIndex]
            vc.overtimeFormView.curMonthNum[cellNum] = vc.userInform.monthsNum[curMonthIndex]
            vc.overtimeFormView.curYearNum[cellNum] = vc.userInform.yearsNum[curYearIndex]
            vc.overtimeFormView.curDayNum[cellNum] = dayNum[curDayIndex]
            vc.overtimeFormView.tabView.reloadData()
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
            if(self.parentView == "leaveView")
            {
                self.vc.leaveFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "overtimeView")
            {
                self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            
        }, completion: {finish in
            self.removeFromSuperview()
            if(self.parentView == "leaveView")
            {
                self.vc.leaveView.alpha = 1
            }
            else if(self.parentView == "overtimeView")
            {
                self.vc.overtimeView.alpha = 1
            }
        })
        
    }
    
    //Cancle Picking----------------------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func backgroundTapped(_ gesture : UITapGestureRecognizer)
    {
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0,options: [], animations: {
            
            self.alpha = 0
            self.transform = CGAffineTransform(scaleX: 0.8, y: 0.8)
            
            if(self.parentView == "leaveView")
            {
                self.vc.leaveFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            else if(self.parentView == "overtimeView")
            {
                self.vc.overtimeFormView.transform = CGAffineTransform(scaleX: 1, y: 1)
            }
            
        }, completion: {finish in
            self.removeFromSuperview()
            if(self.parentView == "leaveView")
            {
                self.vc.leaveView.alpha = 1
            }
            else if(self.parentView == "overtimeView")
            {
                self.vc.overtimeView.alpha = 1
            }
        })
    }
    
    //Gesture View Delegate Methods-------------------------------------------------
    //------------------------------------------------------------------------------
    //------------------------------------------------------------------------------
    
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        
        if (touch.view != self && touch.view != pickerView && touch.view != dayTabView && touch.view != monthTabView && touch.view != yearTabView) {
            return false
        }
        else
        {
            return true
        }
    }


}
