//
//  LoadingView.swift
//  Nex_HR
//
//  Created by Daud on 10/23/16.
//  Copyright © 2016 Daud. All rights reserved.
//

import UIKit

class LoadingView: UIView {

    var vc : ViewController!
    let lbl = UILabel()
    
    func initialize(vc : ViewController, label : String)
    {
        self.vc = vc
        self.backgroundColor = UIColor(red: 192 / 255, green: 197 / 255, blue: 199 / 255, alpha: 1)
        lbl.text = label
        lbl.textAlignment = .center
        lbl.textColor = UIColor.white
        lbl.font = UIFont(name: "SFUIText-Bold", size: 18)
        lbl.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        lbl.frame = CGRect(x: 0, y: 0, width: 200, height: 50)
        lbl.sizeToFit()
        self.frame = CGRect(x: 0, y: vc.view.frame.height, width: lbl.frame.width + 60, height: 50)
        lbl.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
        self.addSubview(lbl)
        self.center.x = vc.view.frame.width / 2
        self.layer.cornerRadius = self.frame.height / 2
        
        UIView.animate(withDuration: 0.2, delay: 0, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.vc.view.frame.height - 65
            
            }, completion: nil)
    }
    
    func succeeded()
    {
        lbl.text = "Succeed!"
        lbl.sizeToFit()
        
        vc.view.bringSubview(toFront: self)
        
        UIView.animate(withDuration: 0.1, animations: {
            
            self.backgroundColor = UIColor(red: 0, green: 204/255, blue: 126/255, alpha: 1)
            self.frame.size.width = 140
            self.lbl.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
            self.center.x = self.vc.view.frame.width / 2
            
        })
        
        UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.vc.view.frame.height
            
            }, completion: {finished in
                self.removeFromSuperview()
        })
    }
    
    func failed()
    {
        lbl.text = "Error!"
        lbl.sizeToFit()
        
        UIView.animate(withDuration: 0.1, animations: {
            
            self.backgroundColor = UIColor(red: 255/255, green: 76/255, blue: 64/255, alpha: 1)
            self.frame.size.width = 140
            self.lbl.center = CGPoint(x: self.frame.width / 2, y: self.frame.height / 2)
            self.center.x = self.vc.view.frame.width / 2
            
        })
        
        UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 0.95, initialSpringVelocity: 0,options: [], animations: {
            
            self.frame.origin.y = self.vc.view.frame.height
            
        }, completion: {finished in
            self.removeFromSuperview()
        })
    }

}
